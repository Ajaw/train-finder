package rajwa.com.trainfinder.DatabaseSearch;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import rajwa.com.trainfinder.DAOs.dbConnectionDAO;
import rajwa.com.trainfinder.Model.*;

import rajwa.com.trainfinder.Model.dbConnectedStation;
import rajwa.com.trainfinder.Model.dbConnectionToHour;
import rajwa.com.trainfinder.Model.loaders.SearchData;
import rajwa.com.trainfinder.RailwayModel.SearchHelper;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.queriable.StringQuery;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import rajwa.com.trainfinder.Model.ArrivalHour;
import rajwa.com.trainfinder.Model.dbConnection;
import rajwa.com.trainfinder.Model.dbConnectionMapping;
import rajwa.com.trainfinder.Model.precomputedData.PartView;

/**
 * Created by Dominik on 11.05.2017.
 */

public class SimpleConnectionSearch {

    public List<SearchData> getSearchData() {
        return searchData;
    }

    public void setSearchData(List<SearchData> searchData) {
        this.searchData = searchData;
    }

    private List<SearchData> searchData;

    public List<List<ArrivalHour>> getConnection(final String start, final String finish, List<PartView> views, Date currentDate){

       final Integer startStation = SearchHelper.getStationIdByName(start).id;
       final Integer finishStation = SearchHelper.getStationIdByName(finish).id;




        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        //    List<dbPrecomputedPart> parts = SQLite.select().from(dbPrecomputedPart.class).queryList();
            List<PartView> candidates = new LinkedList<>();

            List<Integer> _stations = new LinkedList<>();

            DateTime dateTime = new DateTime(currentDate);
            int dayOfWeek =  dateTime.getDayOfWeek();
            String exp = "";
            switch (dayOfWeek){
                case 1:
                     exp = "map.monday=1 or";
                    break;
                case 2:
                     exp = "map.tuesday=1 or";
                    break;
                case 3:
                     exp = "map.wenesday=1 or";
                    break;
                case 4:
                     exp = "map.thursday=1 or";
                    break;
                case 5:
                     exp = "map.friday=1 or";
                    break;
                case 6:
                     exp = "map.saturday=1 or";
                    break;
                case 7:
                     exp = "map.sunday=1 or";
                    break;
            }

            StringQuery<dbConnectionMapping> query2 = new StringQuery<>(dbConnectionMapping.class,"SELECT map.* FROM dbConnectionAvalabities ava "+
                    "left join dbConnectionMapping map on map.id=ava.mapping_id " +
                   "where  ("+exp+" ava.ignoreWeekDays=1) and (date('"+dateFormat.format(currentDate)+"') between date(ava.beggingDate/1000, 'unixepoch') and date(ava.endDate/1000, 'unixepoch'))" +
                    "and map.connectionId in (" +
                    "select c1.id from dbConnection c1\n" +
                    "left join dbStationOnConnection cc on "+startStation+"=cc.dbStationId and cc.dbConnectionId=c1.id\n" +
                    "left join dbStationOnConnection cc2 on "+finishStation+"=cc2.dbStationId and cc2.dbConnectionId=c1.id\n" +
                    " where c1.id in ( SELECT c.dbConnectionId FROM dbStationOnConnection c where dbStationId in ("+startStation+","+finishStation+")\n" +
                    "\n" +
                    "  group by c.dbConnectionId having  count(*)=2  ) and cc.id<cc2.id\n" +
                    " )");

            List<dbConnectionMapping> tempMappings = new LinkedList<>(query2.queryList());

            HashMap<Integer,dbConnectionMapping> temMap =new HashMap<>();
//            StringQuery<dbConnectionMapping> queryTest = new StringQuery<>(dbConnectionMapping.class,"SELECT map.* FROM dbConnectionAvalabities ava "+
//                    "left join dbConnectionMapping map on map.id=ava.mapping_id " +
//                    "where  (map.sunday=1 or ava.ignoreWeekDays=1) and (date('2017-11-19') between date(ava.beggingDate/1000, 'unixepoch') and date(ava.endDate/100, 'unixepoch'))" +
//                    "and map.connectionId in(313,\n" +
//                    "314,\n" +
//                    "323,\n" +
//                    "324)");
//            List<dbConnectionMapping> testList = queryTest.queryList();
            for (dbConnectionMapping map : tempMappings){
                temMap.put(map.getId(),map);
            }
            List<SearchData> data = new LinkedList<>();
            this.searchData = data;
            List<dbConnectionMapping> mappings = new LinkedList<>(temMap.values());
            LocalTime compTime = new LocalTime(currentDate);
            List<List<ArrivalHour>> toRet = new LinkedList<>();
            for (dbConnectionMapping mapping :mappings){
                dbConnectionToHour hour = SQLite.select().from(dbConnectionToHour.class).where(dbConnectionToHour_Table.id.eq(mapping.getConnectionToHourId())).querySingle();
                if (hour!=null) {
                    List<ArrivalHour> arrivalHours = hour.getConHoursList();
                    int current = 0;
                    int currentBeg =0;
                    boolean foundBeg = false;
                    for (ArrivalHour arr : arrivalHours) {

                        if (arr.getStationID()==startStation)
                            foundBeg=true;
                        if (arr.getStationID() == finishStation) {
                            current++;
                            break;
                        }
                        if (!foundBeg){
                            currentBeg++;
                        }
                        current++;
                    }
                    LocalDateTime localDateTime = new LocalDateTime(arrivalHours.get(currentBeg).getDeparetureTime());
                    LocalTime time = new LocalTime(localDateTime);
                    if (time.compareTo(compTime)<0)
                        continue;


                   arrivalHours = arrivalHours.subList(currentBeg, current);
                    toRet.add(arrivalHours);
                    SearchData single = new SearchData();
                    single.setArrivalHours(arrivalHours);
                    single.setDate(currentDate);
                    single.setConnection(dbConnectionDAO.getConnectionById(hour.dbConnectionId));
                    this.searchData.add(single);
                }
            }

            _stations.add(startStation);
            _stations.add(finishStation);
//             List<dbPrecomputedSimpleStop> parts = SQLite.select().from(dbPrecomputedSimpleStop.class).where(dbPrecomputedSimpleStop_Table.station_ID.in(_stations)).groupBy(
//                     dbPrecomputedSimpleStop_Table.partID
//             ).having(SQLite.selectCountOf(dbPrecomputedSimpleStop_Table.id.greaterThan(1))).queryList();

//            StringQuery<dbStationOnConnection> query = new StringQuery<>(dbStationOnConnection.class,"SELECT * FROM dbStationOnConnection where dbStationId in ("+startStation+","+finishStation+")   " +
//                    "group by dbConnectionId having count(*)=2 ");
            StringQuery<dbConnection> query = new StringQuery<>(dbConnection.class,"select c1.* from dbConnection c1\n" +
                    "left join dbStationOnConnection cc on "+startStation+"=cc.dbStationId and cc.dbConnectionId=c1.id\n" +
                    "left join dbStationOnConnection cc2 on "+finishStation+"=cc2.dbStationId and cc2.dbConnectionId=c1.id\n" +
                    " where c1.id in ( SELECT c.dbConnectionId FROM dbStationOnConnection c where dbStationId in ("+startStation+","+finishStation+")\n" +
                    "\n" +
                    "  group by c.dbConnectionId having  count(*)=2  ) and cc.id<cc2.id\n" +
                    "  ;\n");
            return toRet;
//            List<dbConnection> connectedStations = query.queryList();
//            List<dbPrecomputedSimpleStop> simpleStops = new LinkedList<>();
//            List<Integer> stopList = new LinkedList<>();
//            for (dbPrecomputedSimpleStop stop : simpleStops){
//                stopList.add(stop.getPartID());
//            }
//
//            //List<dbPrecomputedPart> precomputedParts = SQLite.select().from(dbPrecomputedPart.class).where(dbPrecomputedPart_Table.id.in(stopList)).queryList();
//            List<PartView> tempList = new LinkedList<>();
//            for (PartView view : views){
//                for (dbPrecomputedSimpleStop stop : simpleStops){
//                    if (stop.getPartID()==view.getStops().get(0).getPartID()){
//                        tempList.add(view);
//                        break;
//                    }
//                }
//            }
//
//            for (PartView part : tempList) {
//                try {
//
//                    dbPrecomputedSimpleStop startStop = Iterables.find(part.getStops(), new Predicate<dbPrecomputedSimpleStop>() {
//                        @Override
//                        public boolean apply(dbPrecomputedSimpleStop input) {
//                            return startStation == input.getStation_ID();
//                        }
//                    });
//                    dbPrecomputedSimpleStop finishStop = Iterables.find(part.getStops(), new Predicate<dbPrecomputedSimpleStop>() {
//                        @Override
//                        public boolean apply(dbPrecomputedSimpleStop input) {
//                            return input.getStation_ID() == finishStation;
//                        }
//                    });
//                    part.startIndex = part.getStops().indexOf(startStop);
//                    part.finishIndex = part.getStops().indexOf(finishStop);
//
//                    if (startStop != null && finishStop != null) {
//                        if (part.startIndex<part.finishIndex)
//                            candidates.add(part);
//
//                    }
           }
                catch (Exception ex){
                    System.out.printf(ex.getMessage());
                }

//            Ordering<dbPrecomputedPart> byLength = new Ordering<dbPrecomputedPart>() {
//                @Override
//                public int compare(dbPrecomputedPart left, dbPrecomputedPart right) {
//                    return Ints.compare(left.getStopsOnStation().size(), right.getStopsOnStation().size());
//                }
//            };
//
//        Collections.sort(candidates,byLength);
//        System.out.printf("test");
//            HashSet<List<dbConnectedStation>> connections = new HashSet<>();
//            if (candidates.size()>0){
//            for (PartView candidate : candidates){
//
//            for (dbPrecomputedConnection connection : candidate.getConnections()){
//               List<dbPrecomputedStop> tempStopList = connection.getPrecomputedStops().subList(candidate.startIndex,candidate.finishIndex+1);
//
//                List<dbConnectedStation> tempConnectedStationList = new LinkedList<>();
//                List<Integer> connStationIds = new LinkedList<>();
//                    for (dbPrecomputedStop connStaId : tempStopList){
//                        connStationIds.add( connStaId.getConnectedStationID());
//                    }
//
//                    tempConnectedStationList.addAll(SQLite.select().from(dbConnectedStation.class)
//                            .where(dbConnectedStation_Table.id.in(connStationIds)).queryList());
//
//
//                if (!checkIfConnectionInSet(tempConnectedStationList,connections))
//                    connections.add(tempConnectedStationList);
//            }
//            }
//
//
////            System.out.printf("test");
//            return new LinkedList<>(connections);
//        }
//        }
//        catch (Exception ex){
//            System.out.printf("test");
//        }
//        return null;
        return  new LinkedList<>();
    }

    private boolean checkIfConnectionInSet(List<dbConnectedStation> stopList, HashSet<List<dbConnectedStation>> connections){
        boolean isInSet = false;
        for (List<dbConnectedStation> tempList : connections) {
            boolean possibility = false;
            int i = 0;
            if (stopList.size() == tempList.size()) {
                for (dbConnectedStation stop : tempList) {

                    if (stopList.get(i).id == stop.id)
                        possibility = true;
                    else {
                        possibility = false;
                        break;
                    }
                    i++;
                }
                if (possibility)
                    return true;
                else
                    isInSet=false;

            }
        }
        return false;
    }

}
