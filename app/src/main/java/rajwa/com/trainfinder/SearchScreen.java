package rajwa.com.trainfinder;
import rajwa.com.trainfinder.Fragments.map;
import rajwa.com.trainfinder.Fragments.search_main;
import rajwa.com.trainfinder.Model.precomputedData.PartView;

import rajwa.com.trainfinder.R;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;


import java.util.List;

public class SearchScreen extends AppCompatActivity {


    public boolean loadingFinished;
    public List<PartView> views;

    private Fragment fragment;
    Fragment currentFragment = null;
    FragmentTransaction ft;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        try {


            loadingFinished = false;

            new Thread(new Runnable() {
                @Override
                public void run() {
//                    views = SQLite.select().from(PartView.class).queryList();
//                    for (PartView view : views){
//                        view.getStops();
//                    }
                    // DataStorage storage = new DataStorage(views);
                    loadingFinished = true;
                }
            }).run();

            super.onCreate(savedInstanceState);
            getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
            getSupportActionBar().hide();

            ft = getSupportFragmentManager().beginTransaction();
            currentFragment = new search_main();
            ft.replace(R.id.search_main, currentFragment);
            ft.commit();

            setContentView(R.layout.activity_search_screen);
            BottomNavigationView bottomNavigationMenu = (BottomNavigationView) findViewById(R.id.bottom_navigation);

            bottomNavigationMenu.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()){
                        case R.id.action_wyszukaj:
                            currentFragment = new search_main();
                            ft = getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.search_main, currentFragment);
                            ft.commit();
                            break;
                        case R.id.action_stations:
                            currentFragment = new map();
                            ft = getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.search_main, currentFragment);
                            ft.commit();
                            break;
//                        case R.id.action_favourites:
//                            break;
                    }
                    return true;
                }
            });


        }
        catch (Exception ex){
            System.console().printf(ex.getMessage());
        }
    }


}
