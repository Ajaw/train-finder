package rajwa.com.trainfinder.UpdateData;

import rajwa.com.trainfinder.Model.dbScheduleVersion;

import java.util.List;

/**
 * Created by Dominik on 04.12.2017.
 */

public interface DataCallback {

    void onSuccess(List<dbScheduleVersion> result);

}
