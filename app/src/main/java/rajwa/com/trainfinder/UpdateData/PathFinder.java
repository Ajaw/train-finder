package rajwa.com.trainfinder.UpdateData;

import rajwa.com.trainfinder.Model.dbConnectedStation;
import rajwa.com.trainfinder.Model.dbConnection;
import rajwa.com.trainfinder.Model.dbStation;
import rajwa.com.trainfinder.Model.*;
import rajwa.com.trainfinder.RailwayModel.structure.ParentStructure;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import rajwa.com.trainfinder.Model.dbStationOnConnection;

/**
 * Created by Dominik on 14.04.2017.
 */

public class PathFinder {

    private int numberOfSwitches;

    private dbStation startStation;

    private dbStation finishStation;

    private List<dbStation> stationRepository;

    private List<dbConnectedStation> list;

    List<ParentStructure> queue;

    private List<dbConnectedStation> stationOnConnections;

    public List<List<dbConnectedStation>> allRoutes;

    private Set<List<dbConnectedStation>> mylist;

    public void dfs(dbConnection connection){
        try {
            stationRepository = SQLite.select().from(dbStation.class).queryList();
            dbStationOnConnection station = connection.getStationList().get(0);
            dbStation st = getStationByID(station.dbStationId);

            dbConnectedStation conStation =  getConnectedStation(connection, st);
            dbStationOnConnection forwardStation = connection.getStationList().get(1);
            st = getStationByID(forwardStation.dbStationId);
            allRoutes = new ArrayList<>();

            queue = new ArrayList<>();
            List<dbConnectedStation> connectedStations = new LinkedList<>();
            connectedStations.addAll(st.getDirectStationConnection());

            stationOnConnections = new ArrayList<>();
            numberOfSwitches =2;
            loop(connectedStations, conStation, 0, null,true);

//            if(stationOnConnections.size()==0){
//                numberOfSwitches = 3;
//                loop(connectedStations, null, 0, null,true);
//            }


            retriveDataFromSearch();

            System.out.printf("etst");


        }
        catch (Exception ex){
            System.out.printf("test");
        }
    }

    private dbConnectedStation getConnectedStation(dbConnection connection, dbStation st) {
        for (dbConnectedStation connectedStation : st.getDirectStationConnection()){
            if (connectedStation.connectionID == connection.getId()){
                return connectedStation;
            }
        }
        return null;
    }

    private void retriveDataFromSearch(){
        for (ParentStructure structure : queue){
            if (structure.isFinalStation()){
                retraceRoute(structure);
            }
        }

        mylist = new HashSet<>(allRoutes);
    }

    private void retraceRoute(ParentStructure p){
        List<dbConnectedStation> route = new ArrayList<>();
        do {
            route.add(p.getStation());

            p = p.getStructure();
        }  while (p.getStructure()!=null);
        route.add(p.getStation());
        route.add(p.getParentStation());
        allRoutes.add(reversList(route));


        System.out.printf("etst");
    }



    private void loop(List<dbConnectedStation> connectedStations,dbConnectedStation previousStation,int numer_of_switches,ParentStructure structure,boolean first_flag){
        //  List<dbStationOnConnection> stationOnConnections = SQLite.select().from(dbStationOnConnection.class).queryList();


        int number_of_iter = connectedStations.size();
        int previous_number_of_switches = numer_of_switches;
        try {
            ParentStructure previousStructer = structure;
            for (int i = 0; i < number_of_iter; i++) {
                numer_of_switches = previous_number_of_switches;
                if (first_flag) {
                    previousStructer = null;
//                    if (connectedStations.get(i).previousStationID!=0) {
//                        previousStation = getConnectedStationById(connectedStations.get(i).previousStationID);
//                    }
//                    else {
//                        previousStation=null;
//                    }
                 //  previousStation =null;
                    List<dbConnectedStation> list1 = getStationByID(connectedStations.get(i).connectedStationID).getDirectStationConnection();
                    if (list1.size() > 0) {
                        ParentStructure p = new ParentStructure(previousStation,connectedStations.get(i));

                            if (connectedStations.get(i).connectionID != previousStation.connectionID) {
                                numer_of_switches++;
                                //continue;
                            }

                        loop(list1, connectedStations.get(i),
                                numer_of_switches, p, false);

                    }
                    //connectedStations.get(i).visited = true;
                }
                else {

                        if (connectedStations.get(i).connectionID != previousStation.connectionID) {
                            numer_of_switches++;
                            //continue;
                        }


                    if (numer_of_switches >= numberOfSwitches) {

                        connectedStations.get(i).visited = true;
                      //  numer_of_switches--;
                        continue;

                    } else if (connectedStations.get(i).finalStation && numer_of_switches > 0 && !(connectedStations.get(i).visited)) {
                        connectedStations.get(i).visited = true;
                        stationOnConnections.add(connectedStations.get(i));

                        ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));
                        p.setFinalStation(true);
                        p.setStructure(previousStructer);
                        retraceRoute(p);
                        queue.add(p);
                        break;
                    }
//                    } else if (connectedStations.get(i).finalStation && numer_of_switches > 0 &&(connectedStations.get(i).visited)) {
//                        connectedStations.get(i).visited = true;
//                        stationOnConnections.add(connectedStations.get(i));
//
//                        ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));
//                        p.setFinalStation(true);
//                        p.setStructure(previousStructer);
//                        queue.add(p);
//                        System.out.printf("te");
//
//
//                    }
                        else {
                        List<dbConnectedStation> list1 = getStationByID(connectedStations.get(i).connectedStationID).getDirectStationConnection();
                        if (list1.size() > 0) {
                            ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));

//                            if(first_flag) {
//                                p = new ParentStructure(null, connectedStations.get(i));
//                            }
                            p.setStructure(previousStructer);
                            //queue.add(p);
                            // previousStructer = p;
                            loop(list1, connectedStations.get(i),
                                    numer_of_switches, p, false);

                        }

                    }
                    connectedStations.get(i).visited = true;
                }
//              i  else if (connectedStations.get(i).finalStation && numer_of_switches > 0) {
//                    connectedStations.get(i).visited = true;
//                    stationOnConnections.add(connectedStations.get(i));
//
//                    ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));
//                    p.setFinalStation(true);
//                    p.setStructure(previousStructer);
//                    queue.add(p);
//
//
//                }
            }
        }
        catch (Exception ex){

            System.out.printf("etst");
        }
    }


    private dbConnectedStation getConnectedStationById(int id){
        dbConnectedStation retStatio = new dbConnectedStation();
        for (dbConnectedStation station : list){
            System.out.printf("test");
            if (station.id == id){
                System.out.printf("test");
                retStatio = station;
                break;
            }
        }
        return retStatio;
    }

    private dbStation getStationByID(int id){
        dbStation retStatio = new dbStation();
        for (dbStation station :stationRepository){
            System.out.printf("test");
            if (station.id == id){
                System.out.printf("test");
                retStatio = station;
                break;
            }
        }
        return retStatio;
    }

    private dbStation getStationIdByName(String stationName){
        return  SQLite.select().from(dbStation.class).where(dbStation_Table.stationName.eq(stationName)).queryList().get(0);
    }


    private List<dbConnectedStation> reversList(List<dbConnectedStation> connectedStations){
        Collections.reverse(connectedStations);
        return  connectedStations;
    }

}
