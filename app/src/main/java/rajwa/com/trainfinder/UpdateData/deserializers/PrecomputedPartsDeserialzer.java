package rajwa.com.trainfinder.UpdateData.deserializers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by Dominik on 11.05.2017.
 */

public class PrecomputedPartsDeserialzer<T> implements JsonDeserializer<T> {

    private final Class mNestedClazz;
    private final Object mNestedDeserializer;

    public PrecomputedPartsDeserialzer(Class nestedClass, Object nestedDeserializer){
        mNestedClazz = nestedClass;
        mNestedDeserializer = nestedDeserializer;
    }

    @Override
    public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement content = json.getAsJsonObject().get("connections");

        GsonBuilder builder = new GsonBuilder();
        if(mNestedClazz != null && mNestedDeserializer != null){
            builder.registerTypeAdapter(mNestedClazz,mNestedDeserializer);
        }
        return builder.create().fromJson(content,typeOfT);
    }
}
