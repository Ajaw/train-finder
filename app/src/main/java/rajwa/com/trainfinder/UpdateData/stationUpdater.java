package rajwa.com.trainfinder.UpdateData;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.ModelAdapter;
import com.raizlabs.android.dbflow.structure.database.transaction.FastStoreModelTransaction;

import rajwa.com.trainfinder.Model.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import rajwa.com.trainfinder.Model.dbConnectedStation;
import rajwa.com.trainfinder.Model.dbSimpleStation;
import rajwa.com.trainfinder.TrainFinderApplication;
import rajwa.com.trainfinder.Model.dbConnection;
import rajwa.com.trainfinder.Model.dbStationOnConnection;

/**
 * Created by Dominik on 07.04.2017.
 */

public class stationUpdater {

    private List<dbConnection> connectionList;

    public stationUpdater(){
        connectionList = SQLite.select().from(dbConnection.class).queryList();
    }


    public void updateStationsConnetion(){
        try {



            List<dbConnectedStation> connectedStations = new ArrayList<>();
            dbStationOnConnection nextStation;

            int counter = 1;
            for (dbConnection connection : connectionList) {
                List<dbStationOnConnection> stationOnConnections = connection.getStationList();
                dbConnectedStation previousStation = new dbConnectedStation();
                int i = 0;
                while (stationOnConnections.size() > i) {

                    dbStationOnConnection currentStation = stationOnConnections.get(i);
                    dbConnectedStation connectedStation2 = new dbConnectedStation();
                    if (i != 0) {
                        connectedStation2.previousStationID = previousStation.id;
                    }
                    if (i < stationOnConnections.size()-1) {
                        nextStation = stationOnConnections.get(i+1);
                        connectedStation2.id =counter;
                        connectedStation2.motherStationID = currentStation.dbStationId;
                        connectedStation2.connectedStationID = nextStation.dbStationId;
                        connectedStation2.connectionID = currentStation.dbConnectionId;
                        connectedStations.add(connectedStation2);
                        previousStation = connectedStation2;
                    }
                    else {
                        connectedStation2.id =counter;
                        connectedStation2.motherStationID = currentStation.dbStationId;
                        connectedStation2.connectionID = currentStation.dbConnectionId;
                        connectedStation2.finalStation = true;
                        connectedStations.add(connectedStation2);
                    }

                    i++;
                    counter++;
                }

            }

            SQLite.delete(dbConnectedStation.class).executeUpdateDelete();
            ModelAdapter<dbConnectedStation> modelAdapter = FlowManager.getModelAdapter(dbConnectedStation.class);

            TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                    .insertBuilder(modelAdapter).addAll(connectedStations).build());

           makeSimpleStationConnections();

//            List<List<dbConnectedStation>> allRoutes= new LinkedList<>();
//            int currect_pos =1;
//            List<dbConnection> connetionsToLookup = new LinkedList<>();
//            for (dbConnection connection : connectionList) {
//                int postion = contatinsId(connection.getStationList(),419);
//                if (postion!=0){
//                    try {
//                        if (connection.getStationList().get(postion + 2).dbStationId == 318) {
//                            connetionsToLookup.add(connection);
//                        }
//                    }
//                    catch (Exception ex){
//                        System.out.printf("test");
//                    }
//                }
//            }
//
//
//            for (dbConnection connection : connetionsToLookup) {
//                PathFinder finder = new PathFinder();
//                if (connection.getStationList().size()>0) {
//                    finder.dfs(connection);
//                    allRoutes.addAll(finder.allRoutes);
//                }
//                else {
//                    System.out.printf("tes");
//                }
//                currect_pos++;
//            }
//            System.out.printf("tes");
//

//
//
//            SQLite.delete(PrecomputedPath.class).executeUpdateDelete();
//            SQLite.delete(PrecomputedStation.class).executeUpdateDelete();
//
//            counter =1;
//            int counter2=1;
//            ModelAdapter<PrecomputedPath> pathAdapater = FlowManager.getModelAdapter(PrecomputedPath.class);
//            ModelAdapter<PrecomputedStation> patStationhAdapater = FlowManager.getModelAdapter(PrecomputedStation.class);
//
//            for (List<dbConnectedStation> connectedStations1 : allRoutes){
//                PrecomputedPath path = new PrecomputedPath();
//                path.setStationsList(connectedStations1);
//                path.setId(counter);
//                List<PrecomputedStation> precomputedStations = new LinkedList<>();
//                for (dbConnectedStation connectedStation : connectedStations1){
//
//                    PrecomputedStation precomputedStation = new PrecomputedStation();
//                    precomputedStation.setConnectedStationID(connectedStation.id);
//                    precomputedStation.setPrecomputedPathID(counter);
//                    precomputedStation.setId(counter2);
//                    precomputedStations.add(precomputedStation);
//                            counter2++;
//                }
//                TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
//                        .insertBuilder(patStationhAdapater).addAll(precomputedStations).build());
//                TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
//                        .insertBuilder(pathAdapater).add(path).build());
//                counter++;
//            }

            System.out.printf("tes");

        }
        catch (Exception ex){
            System.out.printf("test");
        }

    }

    private  int contatinsId(List<dbStationOnConnection> stations, int id){
        int counter = 0;
        for (dbStationOnConnection station: stations){
            if (station.dbStationId == id){
                System.out.printf("test");
                return counter;
            }
            counter++;
        }
        return 0;
    }

    private void makeSimpleStationConnections(){
        SQLite.delete(dbSimpleStation.class).executeUpdateDelete();
        int counter =0;
        for (dbConnection connection : connectionList) {
            List<dbStationOnConnection> stationOnConnections = connection.getStationList();
            dbSimpleStation previousStation = new dbSimpleStation();
            dbStationOnConnection nextStation = new dbStationOnConnection();

            List<dbSimpleStation> connectedStations = new LinkedList<>();
            int i = 0;

            while (stationOnConnections.size() > i) {

                dbStationOnConnection currentStation = stationOnConnections.get(i);
                dbSimpleStation connectedStation2 = new dbSimpleStation();
                if (i != 0) {
                    connectedStation2.previousStationID = previousStation.motherStationID;
                }
                if (i < stationOnConnections.size()-1) {
                    nextStation = stationOnConnections.get(i+1);
                    connectedStation2.id =counter;
                    connectedStation2.motherStationID = currentStation.dbStationId;
                    connectedStation2.connectedStationID = nextStation.dbStationId;
                    if(previousStation.motherStationID!=0 || nextStation.dbStationId!=0) {
                        if (!checkIfSimpleStationExists(currentStation.dbStationId, nextStation.dbStationId)) {
                            connectedStations.add(connectedStation2);
                            previousStation = connectedStation2;
                            counter++;
                        }
                    }
                }
//                else {
//                    connectedStation2.id =counter;
//                    connectedStation2.motherStationID = currentStation.dbStationId;
//                    if (previousStation.motherStationID!=0) {
//                        connectedStations.add(connectedStation2);
//                        counter++;
//                    }
//                }

                i++;

            }


            ModelAdapter<dbSimpleStation> modelAdapter = FlowManager.getModelAdapter(dbSimpleStation.class);
            TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                    .insertBuilder(modelAdapter).addAll(connectedStations).build());

        }
    }

    private boolean checkIfSimpleStationExists(int motherStation, int connectedStation){
        try {
            List<dbSimpleStation> station = SQLite.select().from(dbSimpleStation.class).where(dbSimpleStation_Table.motherStationID.eq(motherStation)).and(
                    dbSimpleStation_Table.connectedStationID.eq(connectedStation)).queryList();
            if (station.size() > 0) {
                return true;
            } else
                return false;
        }
        catch (Exception ex){
            return  false;
        }
    }

}
