package rajwa.com.trainfinder.RecyclerViews.Adapters;

import com.bignerdranch.expandablerecyclerview.ClickListeners.ParentItemClickListener;
import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import com.google.common.collect.Iterables;

import rajwa.com.trainfinder.Activities.ResultDisplay;
import rajwa.com.trainfinder.Model.ArrivalHour;
import rajwa.com.trainfinder.Model.dbConnection;

import rajwa.com.trainfinder.R;
import rajwa.com.trainfinder.RecyclerViews.ExpandableRecyclerAdapter2;
import rajwa.com.trainfinder.RecyclerViews.Model.SearchParent;
import rajwa.com.trainfinder.RecyclerViews.ViewHolders.LoadingViewHolder;
import rajwa.com.trainfinder.RecyclerViews.ViewHolders.SearchChildViewHolder;
import rajwa.com.trainfinder.RecyclerViews.ViewHolders.SearchParentViewHolder;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

import hirondelle.date4j.DateTime;
import rajwa.com.trainfinder.DAOs.dbStationDAO;

/**
 * Created by Dominik on 01.05.2017.
 */

public class Adapter extends ExpandableRecyclerAdapter2<SearchParentViewHolder,SearchChildViewHolder> {

    public OnLoadMoreListener getOnLoadMoreListener() {
        return onLoadMoreListener;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private String lackOfDataString="B.d.";

    public ResultDisplay resultDisplay;

    private OnLoadMoreListener onLoadMoreListener;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private int visibleThreshold = 1;
    LayoutInflater inflater;

    private List<ParentObject> objectData;

    public void setLoaded(){
        loading=false;
    }

    public Adapter(Context context, List<ParentObject> parentItemList,List<Object> objectData, RecyclerView recyclerView) {
        super(context, parentItemList);
        this.mItemList = objectData;

        this.mParentItemList = parentItemList;
         final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                .getLayoutManager();
        inflater = LayoutInflater.from(context);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!loading && totalItemCount <= lastVisibleItem +visibleThreshold){
                    if (onLoadMoreListener!=null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    loading=true;
                }
            }
        });

    }

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 2;


    @Override
    public RecyclerView.ViewHolder onCreateParentViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder vh;
       if (viewType == VIEW_TYPE_ITEM) {
           View view = inflater.inflate(R.layout.parentresultview, viewGroup, false);

           vh = new SearchParentViewHolder(view);
       }
       else  {
           View view = inflater.inflate(R.layout.layout_loading_item, viewGroup, false);

           vh = new LoadingViewHolder(view);
       }
       return  vh;
    }

    @Override
    public SearchChildViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {


        View view = inflater.inflate(R.layout.child_result_view,viewGroup,false);
        return new SearchChildViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(RecyclerView.ViewHolder holder, int b, Object o) {

        if (holder instanceof SearchParentViewHolder) {
            final SearchParentViewHolder searchParentViewHolder = (SearchParentViewHolder) holder;
            final List<ParentObject> parnetObjects = this.mParentItemList;
            //final List<Object> allObjects = this.mItemList;
            final SearchParent parent = (SearchParent) o;

            searchParentViewHolder.setParentItemClickListener(new ParentItemClickListener() {
                @Override
                public void onParentItemClickListener(int i) {
                    try {
                        if (!loading) {
//                    Integer indexDiff = mItemList.size()- mParentItemList.size();
//                    Log.e("INDEX DIFF:",indexDiff.toString());
//                    Log.e("POSITION:",String.valueOf(i));
//                     i = i-indexDiff;
//                    Log.e("FINAL POSTION:",String.valueOf(i));

                            ParentObject p = (ParentObject) resultDisplay.objectData.get(i);
                            Adapter.super.expandParent(parent, i);
                            TextView textView = (TextView) searchParentViewHolder.itemView.findViewById(R.id.arrow);
                            Drawable[] drawables = textView.getCompoundDrawables();
                            searchParentViewHolder.setBack(!searchParentViewHolder.isBack());
                            if (drawables.length > 0) {

//                        ObjectAnimator imageViewObjectAnimator = ObjectAnimator.ofFloat(textView,
//                                "rotation", 0f, 180f);
//
//
//
//
                                if (searchParentViewHolder.repeatCount % 2 != 0) {
                                    textView.animate().rotation(180f).setDuration(300).start();
//                           imageViewObjectAnimator =  ObjectAnimator.ofFloat(textView,
//                                    "rotation", 0f, -180f);
                                } else {
                                    textView.animate().rotation(0f).setDuration(300).start();
                                }
                                searchParentViewHolder.repeatCount = searchParentViewHolder.repeatCount + 1;
//
//    // Set the animation's parameters
//
//                        imageViewObjectAnimator.setDuration(300); // miliseconds
//                        imageViewObjectAnimator.start();
                            }
                        }
                    }
                    catch (Exception ex){
                        if (ex.getMessage()!=null) {
                            Log.e("Exception Loading", ex.getMessage());
                        }
                        else {
                            Log.e("Exception Loading", "ex loading");
                        }
                    }
                }
            });
//        ConstraintLayout myView = (ConstraintLayout) searchParentViewHolder.itemView.findViewById(R.id.parentLayout);
//        myView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                TextView textView = (TextView) v.findViewById(R.id.arrow);
//                Drawable[] drawables = textView.getCompoundDrawables();
//                if(drawables.length>0) {
//
//                    Animation an = new RotateAnimation(0.0f, 180.0f);
//
//// Set the animation's parameters
//                    ObjectAnimator imageViewObjectAnimator = ObjectAnimator.ofFloat(textView ,
//                            "rotation", 0f, 180f);
//                    imageViewObjectAnimator.setDuration(1000); // miliseconds
//                    imageViewObjectAnimator.start();
//                }
//
//            }
//        });


            Format formatter = new SimpleDateFormat("HH:mm");
            dbConnection connection = parent.getConnection();
            ArrivalHour firts = (ArrivalHour) parent.getChildObjectList().get(0);
            ArrivalHour last = (ArrivalHour) Iterables.getLast(parent.getChildObjectList());

            Format dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            String depBegin = formatter.format(firts.getDeparetureTime());
            String arrBegin = formatter.format(firts.getArrivalTime());
            String currentDate = dateFormat.format(parent.getDate());
            String arrEnd = formatter.format(last.getArrivalTime());
            String stationBegin = dbStationDAO.getStationById(firts.stationID).stationName;
            String stationEnd = dbStationDAO.getStationById(last.stationID).stationName;
            if (depBegin.equalsIgnoreCase("00:00"))
                depBegin = lackOfDataString;
            searchParentViewHolder._departureTime.setText(depBegin);
            if (arrEnd.equalsIgnoreCase("00:00"))
                arrEnd = lackOfDataString;
            searchParentViewHolder._arrivalTime.setText(arrEnd);
            searchParentViewHolder._currentDate.setText(dateFormat.format(parent.getDate()));
            searchParentViewHolder._departureStation.setText(stationBegin);
            searchParentViewHolder._arrivalStation.setText(stationEnd);
            searchParentViewHolder._date.setText(dbStationDAO.getStationById(connection.getStationList().get(0).dbStationId).stationName + " - " +
                    dbStationDAO.getStationById(connection.getStationList().get(connection.getStationList().size() - 1).dbStationId).stationName);
        }
        else if (holder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public void onBindChildViewHolder(SearchChildViewHolder searchChildViewHolder, int i, Object o) {
        try {


            ArrivalHour arrivalHour = (ArrivalHour) o;



            Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


            String s = formatter.format(arrivalHour.getDeparetureTime());
            DateTime time = new DateTime(s);
            String timePickerText = time.format("hh:mm");
            if (timePickerText.equalsIgnoreCase("00:00"))
                timePickerText = lackOfDataString;
            searchChildViewHolder.departureTime.setText(timePickerText);


            s = formatter.format(arrivalHour.getArrivalTime());
             time = new DateTime(s);
            timePickerText = time.format("hh:mm");
            if (timePickerText.equalsIgnoreCase("00:00"))
                timePickerText = lackOfDataString;
            searchChildViewHolder.arrivalTime.setText(timePickerText);

            searchChildViewHolder.station.setText(dbStationDAO.getStationById(arrivalHour.stationID).stationName);
            searchChildViewHolder.platform.setText(arrivalHour.getPlatform());
            searchChildViewHolder.track.setText(arrivalHour.getTrack());
            //searchChildViewHolder.station.setText(dbStationDAO.getStationById(arrivalHour.stationID).stationName + " Peron:" + arrivalHour.getPlatform());
        }
        catch (Exception ex){
            System.out.printf("test");
        }
    }

}
