package rajwa.com.trainfinder.RecyclerViews.Model;

import rajwa.com.trainfinder.Model.dbConnection;

import com.bignerdranch.expandablerecyclerview.Model.ParentObject;

import java.util.Date;
import java.util.List;

/**
 * Created by Dominik on 04.05.2017.
 */

public class SearchParent implements ParentObject {

    public List<Object> mChildrenList;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    private Date date;


    public dbConnection getConnection() {
        return connection;
    }

    public void setConnection(dbConnection connection) {
        this.connection = connection;
    }

    private dbConnection connection;

    @Override
    public List<Object> getChildObjectList() {
        return mChildrenList;
    }

    @Override
    public void setChildObjectList(List<Object> list) {
        mChildrenList = list;
    }
}
