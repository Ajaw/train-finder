package rajwa.com.trainfinder.RecyclerViews.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import rajwa.com.trainfinder.R;
/**
 * Created by Dominik on 05.12.2017.
 */

public  class LoadingViewHolder extends RecyclerView.ViewHolder {

    public ProgressBar progressBar;

    public LoadingViewHolder(View itemView) {
        super(itemView);
        progressBar = (ProgressBar) itemView.findViewById(R.id.loading_search_progress);
    }

}
