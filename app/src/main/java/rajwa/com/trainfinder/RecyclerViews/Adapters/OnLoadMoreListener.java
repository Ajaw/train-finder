package rajwa.com.trainfinder.RecyclerViews.Adapters;

/**
 * Created by Dominik on 05.12.2017.
 */

public interface OnLoadMoreListener {

        void onLoadMore();

}
