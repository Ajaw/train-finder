package rajwa.com.trainfinder.RecyclerViews;

import rajwa.com.trainfinder.RecyclerViews.ViewHolders.LoadingViewHolder;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapterHelper;
import com.bignerdranch.expandablerecyclerview.ClickListeners.ParentItemClickListener;
import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import com.bignerdranch.expandablerecyclerview.Model.ParentWrapper;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Dominik on 30.11.2017.
 */
    public abstract class ExpandableRecyclerAdapter2<PVH extends ParentViewHolder, CVH extends ChildViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ParentItemClickListener {
        private static final String TAG = com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter.class.getClass().getSimpleName();
        private static final String STABLE_ID_MAP = "ExpandableRecyclerAdapter.StableIdMap";
        private static final String STABLE_ID_LIST = "ExpandableRecyclerAdapter.StableIdList";
        private static final int TYPE_PARENT = 0;
        private static final int TYPE_CHILD = 1;
        public static final int CUSTOM_ANIMATION_VIEW_NOT_SET = -1;
        public static final long DEFAULT_ROTATE_DURATION_MS = 200L;
        public static final long CUSTOM_ANIMATION_DURATION_NOT_SET = -1L;
        protected Context mContext;
        public List<Object> mItemList;
        public List<ParentObject> mParentItemList;
        private HashMap<Long, Boolean> mStableIdMap;
        public ExpandableRecyclerAdapterHelper mExpandableRecyclerAdapterHelper;
        private boolean mParentAndIconClickable = false;
        private int mCustomParentAnimationViewId = -1;
        private long mAnimationDuration = -1L;

        public ExpandableRecyclerAdapter2(Context context, List<ParentObject> parentItemList) {
            this.mContext = context;
            this.mParentItemList = parentItemList;
            this.mItemList = this.generateObjectList(parentItemList);
            this.mExpandableRecyclerAdapterHelper = new ExpandableRecyclerAdapterHelper(this.mItemList);
            this.mStableIdMap = this.generateStableIdMapFromList(this.mExpandableRecyclerAdapterHelper.getHelperItemList());
        }

        public ExpandableRecyclerAdapter2(Context context, List<ParentObject> parentItemList, int customParentAnimationViewId) {
            this.mContext = context;
            this.mParentItemList = parentItemList;
            this.mItemList = this.generateObjectList(parentItemList);
            this.mExpandableRecyclerAdapterHelper = new ExpandableRecyclerAdapterHelper(this.mItemList);
            this.mStableIdMap = this.generateStableIdMapFromList(this.mExpandableRecyclerAdapterHelper.getHelperItemList());
            this.mCustomParentAnimationViewId = customParentAnimationViewId;
        }

        public ExpandableRecyclerAdapter2(Context context, List<ParentObject> parentItemList, int customParentAnimationViewId, long animationDuration) {
            this.mContext = context;
            this.mParentItemList = parentItemList;
            this.mItemList = this.generateObjectList(parentItemList);
            this.mExpandableRecyclerAdapterHelper = new ExpandableRecyclerAdapterHelper(this.mItemList);
            this.mStableIdMap = this.generateStableIdMapFromList(this.mExpandableRecyclerAdapterHelper.getHelperItemList());
            this.mCustomParentAnimationViewId = customParentAnimationViewId;
            this.mAnimationDuration = animationDuration;
        }

        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            if(viewType == 0 || viewType==2) {
                if (viewType ==0) {
                    ParentViewHolder pvh = (ParentViewHolder) this.onCreateParentViewHolder(viewGroup,0);
                    pvh.setParentItemClickListener(this);
                    return pvh;
                }
                else  {
                    LoadingViewHolder ldvh = (LoadingViewHolder) this.onCreateParentViewHolder(viewGroup,2);
                    return ldvh;
                }

            } else if(viewType == 1) {
                return this.onCreateChildViewHolder(viewGroup);
            } else {
                LoadingViewHolder ldvh = (LoadingViewHolder) this.onCreateParentViewHolder(viewGroup,2);
                return ldvh;
            }
        }

        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof LoadingViewHolder){
                System.out.printf("test");
                return;
            }

            if( this.mExpandableRecyclerAdapterHelper.getHelperItemAtPosition(position) instanceof ParentWrapper) {
                ParentViewHolder parentViewHolder = (ParentViewHolder)holder;
                if(this.mParentAndIconClickable) {
                    if(this.mCustomParentAnimationViewId != -1 && this.mAnimationDuration != -1L) {
                        parentViewHolder.setCustomClickableViewAndItem(this.mCustomParentAnimationViewId);
                        parentViewHolder.setAnimationDuration(this.mAnimationDuration);
                    } else if(this.mCustomParentAnimationViewId != -1) {
                        parentViewHolder.setCustomClickableViewAndItem(this.mCustomParentAnimationViewId);
                        parentViewHolder.cancelAnimation();
                    } else {
                        parentViewHolder.setMainItemClickToExpand();
                    }
                } else if(this.mCustomParentAnimationViewId != -1 && this.mAnimationDuration != -1L) {
                    parentViewHolder.setCustomClickableViewOnly(this.mCustomParentAnimationViewId);
                    parentViewHolder.setAnimationDuration(this.mAnimationDuration);
                } else if(this.mCustomParentAnimationViewId != -1) {
                    parentViewHolder.setCustomClickableViewOnly(this.mCustomParentAnimationViewId);
                    parentViewHolder.cancelAnimation();
                } else {
                    parentViewHolder.setMainItemClickToExpand();
                }

                parentViewHolder.setExpanded(((ParentWrapper)this.mExpandableRecyclerAdapterHelper.getHelperItemAtPosition(position)).isExpanded());
                this.onBindParentViewHolder((PVH) parentViewHolder, position, this.mItemList.get(position));
            } else {
                if (holder instanceof ParentViewHolder){
                    return;
                }
                if(this.mItemList.get(position) == null) {
                    Integer a = 1;
                    return;
                }

                this.onBindChildViewHolder((CVH) holder, position, this.mItemList.get(position));
            }

        }

        public abstract RecyclerView.ViewHolder onCreateParentViewHolder(ViewGroup var1,int viewType);

        public abstract CVH onCreateChildViewHolder(ViewGroup var1);

        public abstract void onBindParentViewHolder(RecyclerView.ViewHolder var1, int var2, Object var3);

        public abstract void onBindChildViewHolder(CVH var1, int var2, Object var3);

        public int getItemCount() {
            return this.mItemList.size();
        }

        public int getItemViewType(int position) {
            if(this.mItemList.get(position) instanceof ParentObject) {
                return 0;
            } else if(this.mItemList.get(position) == null) {
                return 2;
            } else {
                return 1;
            }
        }

        public void onParentItemClickListener(int position) {
            if(this.mItemList.get(position) instanceof ParentObject) {
                ParentObject parentObject = (ParentObject)this.mItemList.get(position);
                this.expandParent(parentObject, position);
            }

        }

        public void setParentClickableViewAnimationDefaultDuration() {
            this.mAnimationDuration = 200L;
        }

        public void setParentClickableViewAnimationDuration(long animationDuration) {
            this.mAnimationDuration = animationDuration;
        }

        public void setCustomParentAnimationViewId(int customParentAnimationViewId) {
            this.mCustomParentAnimationViewId = customParentAnimationViewId;
        }

        public void setParentAndIconExpandOnClick(boolean parentAndIconClickable) {
            this.mParentAndIconClickable = parentAndIconClickable;
        }

        public void removeAnimation() {
            this.mCustomParentAnimationViewId = -1;
            this.mAnimationDuration = -1L;
        }

        public void expandParent(ParentObject parentObject, int position) {
            ParentWrapper parentWrapper = (ParentWrapper)this.mExpandableRecyclerAdapterHelper.getHelperItemAtPosition(position);
            if(parentWrapper != null) {
                List childObjectList;
                int i;
                if(parentWrapper.isExpanded()) {
                    parentWrapper.setExpanded(false);
                    this.mStableIdMap.put(Long.valueOf(parentWrapper.getStableId()), Boolean.valueOf(false));
                    childObjectList = ((ParentObject)parentWrapper.getParentObject()).getChildObjectList();
                    if(childObjectList != null) {
                        for(i = childObjectList.size() - 1; i >= 0; --i) {
                            this.mItemList.remove(position + i + 1);
                            this.mExpandableRecyclerAdapterHelper.getHelperItemList().remove(position + i + 1);
                            this.notifyItemRemoved(position + i + 1);
                            Log.d(TAG, "Removed " + childObjectList.get(i).toString());
                        }
                    }
                } else {
                    parentWrapper.setExpanded(true);
                    this.mStableIdMap.put(Long.valueOf(parentWrapper.getStableId()), Boolean.valueOf(true));
                    childObjectList = ((ParentObject)parentWrapper.getParentObject()).getChildObjectList();
                    if(childObjectList != null) {
                        for(i = 0; i < childObjectList.size(); ++i) {
                            this.mItemList.add(position + i + 1, childObjectList.get(i));
                            this.mExpandableRecyclerAdapterHelper.getHelperItemList().add(position + i + 1, childObjectList.get(i));
                            this.notifyItemInserted(position + i + 1);
                        }
                    }
                }

            }
        }

        private HashMap<Long, Boolean> generateStableIdMapFromList(List<Object> itemList) {
            HashMap parentObjectHashMap = new HashMap();

            for(int i = 0; i < itemList.size(); ++i) {
                if(itemList.get(i) != null) {
                    ParentWrapper parentWrapper = (ParentWrapper)this.mExpandableRecyclerAdapterHelper.getHelperItemAtPosition(i);
                    parentObjectHashMap.put(Long.valueOf(parentWrapper.getStableId()), Boolean.valueOf(parentWrapper.isExpanded()));
                }
            }

            return parentObjectHashMap;
        }

        private ArrayList<Object> generateObjectList(List<ParentObject> parentObjectList) {
            ArrayList objectList = new ArrayList();
            Iterator var3 = parentObjectList.iterator();

            while(var3.hasNext()) {
                ParentObject parentObject = (ParentObject)var3.next();
                objectList.add(parentObject);
            }

            return objectList;
        }

        public Bundle onSaveInstanceState(Bundle savedInstanceStateBundle) {
            savedInstanceStateBundle.putSerializable("ExpandableRecyclerAdapter.StableIdMap", this.mStableIdMap);
            return savedInstanceStateBundle;
        }



        public void onRestoreInstanceState(Bundle savedInstanceStateBundle) {
            if(savedInstanceStateBundle != null) {
                if(savedInstanceStateBundle.containsKey("ExpandableRecyclerAdapter.StableIdMap")) {
                    this.mStableIdMap = (HashMap)savedInstanceStateBundle.getSerializable("ExpandableRecyclerAdapter.StableIdMap");

                    for(int i = 0; i < this.mExpandableRecyclerAdapterHelper.getHelperItemList().size(); ++i) {
                        if(this.mExpandableRecyclerAdapterHelper.getHelperItemAtPosition(i) instanceof ParentWrapper) {
                            ParentWrapper parentWrapper = (ParentWrapper)this.mExpandableRecyclerAdapterHelper.getHelperItemAtPosition(i);
                            if(this.mStableIdMap.containsKey(Long.valueOf(parentWrapper.getStableId()))) {
                                parentWrapper.setExpanded(((Boolean)this.mStableIdMap.get(Long.valueOf(parentWrapper.getStableId()))).booleanValue());
                                if(parentWrapper.isExpanded()) {
                                    List childObjectList = ((ParentObject)parentWrapper.getParentObject()).getChildObjectList();
                                    if(childObjectList != null) {
                                        for(int j = 0; j < childObjectList.size(); ++j) {
                                            ++i;
                                            this.mItemList.add(i, childObjectList.get(j));
                                            this.mExpandableRecyclerAdapterHelper.getHelperItemList().add(i, childObjectList.get(j));
                                        }
                                    }
                                }
                            } else {
                                parentWrapper.setExpanded(false);
                            }
                        }
                    }

                    this.notifyDataSetChanged();
                }
            }
        }
    }


