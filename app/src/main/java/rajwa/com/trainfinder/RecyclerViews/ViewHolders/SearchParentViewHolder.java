package rajwa.com.trainfinder.RecyclerViews.ViewHolders;

import rajwa.com.trainfinder.R;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by domin on 04.05.2017.
 */

public class SearchParentViewHolder extends com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder {

    public TextView _arrivalStation;

    public  TextView _departureStation;

    public TextView _arrivalTime;

    public TextView _departureTime;

    public TextView _date;

    public Integer repeatCount;

    public TextView _currentDate;

    public boolean isBack() {
        return isBack;
    }

    public void setBack(boolean back) {
        isBack = back;
    }

    private boolean isBack;

    public SearchParentViewHolder(View itemView) {
        super(itemView);
        repeatCount = 1;
        _arrivalStation = (TextView)itemView.findViewById(R.id.parentArrivalStation);

        _arrivalTime = (TextView)itemView.findViewById(R.id.parentArrivalHour);

        _currentDate = (TextView) itemView.findViewById(R.id.currentDateParent);

        _departureStation = (TextView)itemView.findViewById(R.id.parentDepartureStation);

        _departureTime = (TextView)itemView.findViewById(R.id.parentDepartureHour);

        _date = (TextView)itemView.findViewById(R.id.Connection);

    }


}
