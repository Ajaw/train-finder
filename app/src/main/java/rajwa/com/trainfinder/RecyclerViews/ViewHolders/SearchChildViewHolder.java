package rajwa.com.trainfinder.RecyclerViews.ViewHolders;

import rajwa.com.trainfinder.R;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;

import org.w3c.dom.Text;

/**
 * Created by domin on 04.05.2017.
 */

public class SearchChildViewHolder extends ChildViewHolder {

   public TextView station;
    public TextView departureTime;
   public   TextView arrivalTime;
    public TextView platform;
    public TextView track;


    public SearchChildViewHolder(View itemView) {
        super(itemView);
        station = (TextView)itemView.findViewById(R.id.childStation);

        departureTime = (TextView)itemView.findViewById(R.id.childDepartureTime);

        arrivalTime = (TextView)itemView.findViewById(R.id.childArrivalTime);

        platform = (TextView) itemView.findViewById(R.id.platText);

        track = (TextView) itemView.findViewById(R.id.trackText);


    }
}
