package rajwa.com.trainfinder.RailwayModel;

import rajwa.com.trainfinder.Model.dbConnectedStation;
import rajwa.com.trainfinder.Model.dbSimpleStation;
import rajwa.com.trainfinder.Model.dbStation;
import rajwa.com.trainfinder.Model.*;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.Collections;
import java.util.List;

/**
 * Created by Dominik on 18.04.2017.
 */

public class SearchHelper {

    public static dbStation getStationByID(int id, List<dbStation> stationRepository){
        dbStation retStatio = new dbStation();
        for (dbStation station :stationRepository){
            System.out.printf("test");
            if (station.id == id){
                System.out.printf("test");
                retStatio = station;
                break;
            }
        }
        return retStatio;
    }

//    private static void retriveDataFromSearch(){
//        for (SimpleParentStructure structure : queue){
//            if (structure.isFinalStation()){
//                retraceRoute(structure);
//            }
//        }
//
//        //   mylist = new HashSet<>(allRoutes);
//    }
//
//    private static void retraceRoute(SimpleParentStructure p, List<List<dbConnectedStation>> allRoutes){
//        List<dbSimpleStation> route = new ArrayList<>();
//        do {
//            route.add(p.getStation());
//
//            p = p.getStructure();
//        }  while (p.getStructure()!=null);
//        // route.add(p.getStation());
//        allRoutes.add(reversList(route));
//
//
//        System.out.printf("etst");
//    }

    public static List<dbSimpleStation> reversList(List<dbSimpleStation> connectedStations){
        Collections.reverse(connectedStations);
        return  connectedStations;
    }

    public static dbConnectedStation getConnectedStation(dbConnectedStation station){
        return SQLite.select().from(dbConnectedStation.class).where(dbConnectedStation_Table.motherStationID.eq(station.connectedStationID),
                dbConnectedStation_Table.connectionID.eq(station.connectionID)).queryList().get(0);
    }




    public static dbStation getStationIdByName(String stationName){
        return  SQLite.select().from(dbStation.class).where(dbStation_Table.stationName.eq(stationName)).queryList().get(0);
    }
}
