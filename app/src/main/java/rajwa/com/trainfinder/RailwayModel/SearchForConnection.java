package rajwa.com.trainfinder.RailwayModel;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import rajwa.com.trainfinder.Model.*;

import rajwa.com.trainfinder.Model.PrecomputedPath;
import rajwa.com.trainfinder.Model.dbConnectedStation;
import rajwa.com.trainfinder.Model.dbConnection;
import rajwa.com.trainfinder.Model.dbConnectionToHour;
import rajwa.com.trainfinder.Model.dbStation;
import rajwa.com.trainfinder.Model.dbStationOnConnection;
import rajwa.com.trainfinder.RailwayModel.structure.ParentStructure;
import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Dominik on 01.04.2017.
 */



public class SearchForConnection {

    private dbStation startStation;

    private dbStation finishStation;

    private List<dbStation> stationRepository;

    private List<dbConnectedStation> list;

    private Context context;

    private int numberOfSwitches;

    public boolean directConnectionSearch(String start, String finish, Context context){
        try {
            startStation = getStationIdByName(start);
            finishStation =  getStationIdByName(finish);
            int startStationId = startStation.id;
            int finishStationId = finishStation.id;
            this.context = context;
            List<dbConnectionToHour> list1 = SQLite.select().from(dbConnectionToHour.class).queryList();
            List<dbStationOnConnection> list2 = SQLite.select().from(dbStationOnConnection.class).queryList();
            List<dbConnection> list3 = SQLite.select().from(dbConnection.class).queryList();
            stationRepository = SQLite.select().from(dbStation.class).queryList();
           // List<PrecomputedStation> stationPaths = SQLite.select().from(PrecomputedStation.class).queryList();
         //   List<PrecomputedPath> paths = SQLite.select().from(PrecomputedPath.class).queryList();
            list = SQLite.select().from(dbConnectedStation.class).queryList();
           Toast toast = Toast.makeText(context,"Json error"+list1.size(),Toast.LENGTH_LONG);
            toast.show();
            List<PrecomputedPath> correctConnections = new LinkedList<>();
//            for (PrecomputedPath path : paths){
//
//                if (contatinsId(path.getStationList(),startStationId)){
//                if (contatinsId(path.getStationList(),finishStationId))
//                    {
//                        correctConnections.add(path);
//                    }
//                }
//
//            }
            System.out.printf("test");
        }
        catch (Exception ex){

            Toast toast = Toast.makeText(context,"Json error"+ex.getMessage(),Toast.LENGTH_LONG);
            toast.show();
        }
    //bfs();
      //  dfs();
        allRoutes = new LinkedList<>();

        List<dbConnectedStation> connectedStations = getStationByID(startStation.id).getDirectStationConnection();
        numberOfSwitches =2;
        for (dbConnectedStation startStation : connectedStations){
            List<dbConnectedStation> visited = new LinkedList<>();
            visited.add(startStation);
            depthFirst(visited,0);
        }

        return true;
    }

    private   Set<List<dbConnectedStation>> mylist;

    private void dfs(){
        try {
            List<dbConnectedStation> connectedStations = getStationByID(startStation.id).getDirectStationConnection();


            queue = new ArrayList<>();

            stationOnConnections = new ArrayList<>();
            numberOfSwitches =2;
            loop(connectedStations, null, 0, null,true);

           if(stationOnConnections.size()==0){
                numberOfSwitches = 7;
                loop(connectedStations, null, 0, null,true);
            }

            allRoutes = new ArrayList<>();
            retriveDataFromSearch();

            System.out.printf("etst");


        }
        catch (Exception ex){
            System.out.printf("test");
        }
    }

    private void bfs2(){

    }

    private void bfs(){

        List<dbConnectedStation> connectedStations = getStationByID(startStation.id).getDirectStationConnection();
        ParentStructure p;
        for (dbConnectedStation station : connectedStations){
            p = new ParentStructure(null, station);
            p.setStructure(null);
            station.structure = p;
        }
        queue = new LinkedList<>();
        LinkedList<dbConnectedStation> nextToVisit = new LinkedList<>();
        HashSet<Integer> visited = new HashSet<>();
        nextToVisit.addAll(connectedStations);
        dbConnectedStation currentStation;
        ParentStructure previousStructer = new ParentStructure(null,null);
        int number_of_switches =0;
        dbConnectedStation previousStation = new dbConnectedStation();

        while (!nextToVisit.isEmpty() ){

             currentStation = nextToVisit.remove();


            if (currentStation.motherStationID == finishStation.id){
                System.out.printf("test");
                 p = new ParentStructure(currentStation.structure.station, currentStation);
                p.setFinalStation(true);
                p.setStructure(currentStation.structure);
                queue.add(p);
                previousStructer = p;
            }
            else {
                try  {
                    p = new ParentStructure(currentStation.structure.getStation(), currentStation);
                }
                catch (Exception ex){
                    p = new ParentStructure(null, currentStation);
                }

                p.setStructure(currentStation.structure);


            }

            if (visited.contains(currentStation.id)){
                continue;
            }
            else {
                getConnectedStationById(currentStation.id).structure =p;
                currentStation.structure = p;
            }
            visited.add(currentStation.id);

            previousStation = currentStation;
            List<dbConnectedStation> myConStation = getStationByID(currentStation.connectedStationID).getDirectStationConnection();

            for (dbConnectedStation station : myConStation){
                if (previousStation.connectionID != station.connectionID && previousStation.motherStationID!=station.motherStationID){
                    station.number_of_switches = previousStation.number_of_switches+1;
                }
                if (station.number_of_switches >= numberOfSwitches) {
                    visited.add(station.id);
                    continue;

                }
                else {
                    nextToVisit.add(station);
                }
            }


        }

        allRoutes = new ArrayList<>();
        retriveDataFromSearch();

        System.out.printf("etst");




    }



    private List<Integer> extractPath(List<dbConnectedStation> connectedStations){
        List<Integer> path = new ArrayList<>();
        for (dbConnectedStation station : connectedStations){
            path.add(station.motherStationID);
        }

        List<dbConnection> connections = SQLite.select().from(dbConnection.class).queryList();
        Integer station = path.get(0);

//            for (dbConnection connection : connections){
//                if (contatinsId(connection.getStationList(),station) ){
//                    for (Integer station1 : path) {
//                        correctConnections.add(connection);
//                    }
//                }
//            }

        return path;
    }

    private List<dbConnectedStation> reversList(List<dbConnectedStation> connectedStations){
         Collections.reverse(connectedStations);
        return  connectedStations;
    }

    private void retriveDataFromSearch(){
        for (ParentStructure structure : queue){
            if (structure.isFinalStation()){
                 retraceRoute(structure);
            }
        }

       mylist = new HashSet<>(allRoutes);
    }

    private void retraceRoute(ParentStructure p){
        List<dbConnectedStation> route = new ArrayList<>();
    do {
        route.add(p.getStation());

        p = p.getStructure();
    }  while (p.getStructure()!=null);
        route.add(p.getStation());
        allRoutes.add(reversList(route));


        System.out.printf("etst");
    }

    private void depthFirst(List<dbConnectedStation> visited,int currentNumber ){
        List<dbConnectedStation> nodes = getStationByID(visited.get(visited.size()-1).motherStationID).getDirectStationConnection();

        for (dbConnectedStation node : nodes){
            if (visited.contains(node)){
                continue;
            }
            if (node.motherStationID==finishStation.id){
                visited.add(node);
                printPath(visited);
                visited.remove(visited.size()-1);
                break;
            }
        }
        dbConnectedStation previousStation = visited.get(visited.size()-1);
        int tempStore = currentNumber;
        for (dbConnectedStation node : nodes){
            tempStore = currentNumber;
            if (visited.contains(node) || node.motherStationID==finishStation.id){
               continue;
            }
            if (previousStation.connectedStationID!=node.connectionID){
                tempStore++;
            }
            if (currentNumber >= numberOfSwitches){
                continue;
            }
            visited.add(node);
            depthFirst(visited,tempStore);
            visited.remove(visited.size()-1);
        }
    }

    private void printPath(List<dbConnectedStation> visited){
        allRoutes.add(visited);
    }

    private void loop(List<dbConnectedStation> connectedStations,dbConnectedStation previousStation,int numer_of_switches,ParentStructure structure,boolean first_flag){
      //  List<dbStationOnConnection> stationOnConnections = SQLite.select().from(dbStationOnConnection.class).queryList();


        int number_of_iter = connectedStations.size();
        try {
            ParentStructure previousStructer = structure;
            for (int i = 0; i < number_of_iter; i++) {
                if (first_flag) {
                    previousStructer = null;
//                    if (connectedStations.get(i).previousStationID!=0) {
//                        previousStation = getConnectedStationById(connectedStations.get(i).previousStationID);
//                    }
//                    else {
//                        previousStation=null;
//                    }
                    previousStation = null;
                    List<dbConnectedStation> list1 = getStationByID(connectedStations.get(i).connectedStationID).getDirectStationConnection();
                    if (list1.size() > 0) {
                        ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));

                       // queue.add(p);

                        loop(list1, previousStation,
                                numer_of_switches, p, false);

                    }
                    connectedStations.get(i).visited = true;
                }
//                if (previousStation == null) {
//                    ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i) );
//                   // p.setStructure(previousStructer);
//                    queue.add(p);
//                    loop(getStationByID(connectedStations.get(i).connectedStationID
//                            ).getDirectStationConnection(), connectedStations.get(i),
//                            numer_of_switches, p);
//                    connectedStations.get(i).visited = true;
//                } else {
                else  {
                    if (previousStation!=null){
                    if (connectedStations.get(i).connectionID != previousStation.connectionID) {
                        numer_of_switches++;
                    }}

                    if (numer_of_switches >= numberOfSwitches) {

                        connectedStations.get(i).visited = true;
                        numer_of_switches--;


                    } else if (connectedStations.get(i).motherStationID == finishStation.id && !(connectedStations.get(i).visited)) {
                        connectedStations.get(i).visited = true;
                        stationOnConnections.add(connectedStations.get(i));

                        ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));
                        p.setFinalStation(true);
                        p.setStructure(previousStructer);
                        queue.add(p);
                        break;

                    } else if (connectedStations.get(i).motherStationID == finishStation.id && (connectedStations.get(i).visited)) {
                        connectedStations.get(i).visited = true;
                        stationOnConnections.add(connectedStations.get(i));

                        ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));
                        p.setFinalStation(true);
                        p.setStructure(previousStructer);
                        queue.add(p);
                        System.out.printf("te");

                    } else {
                        List<dbConnectedStation> list1 = getStationByID(connectedStations.get(i).connectedStationID).getDirectStationConnection();
                        if (list1.size() > 0) {
                            ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));

//                            if(first_flag) {
//                                p = new ParentStructure(null, connectedStations.get(i));
//                            }
                            p.setStructure(previousStructer);
                            //queue.add(p);
                           // previousStructer = p;
                            loop(list1, connectedStations.get(i),
                                    numer_of_switches, p, false);
                            connectedStations.get(i).visited = true;
                        }
                        connectedStations.get(i).visited = true;
                    }
                    connectedStations.get(i).visited = true;
                }

            }
        }
        catch (Exception ex){
            Toast toast = Toast.makeText(context,"Json error"+ex.getMessage(),Toast.LENGTH_LONG);
            toast.show();
            System.out.printf("etst");
        }
    }



    List<ParentStructure> queue;

    private List<dbConnectedStation> stationOnConnections;

    private List<List<dbConnectedStation>> allRoutes;

//
//    private dbConnectionToHour getArrivalHours(int number, Date hour,int connectionID){
//        List<dbConnectionToHour> foundConnections = SQLite.select().from(dbConnectionToHour.class).where(dbConnectionToHour_Table.dbConnectionId.eq(connectionID)).queryList();
//        for (dbConnectionToHour connectionToHour : foundConnections){
//            List<ArrivalHour> arrivalHours = connectionToHour.getConHoursList();
//            for (ArrivalHour arrivalHour : arrivalHours){
//                arrivalHour.
//            }
//        }
//    }

    private dbConnectedStation getConnectedStationById(int id){
        dbConnectedStation retStatio = new dbConnectedStation();
        for (dbConnectedStation station : list){
            System.out.printf("test");
            if (station.id == id){
                System.out.printf("test");
                retStatio = station;
                break;
            }
        }
        return retStatio;
    }

    private dbStation getStationByID(int id){
        dbStation retStatio = new dbStation();
        for (dbStation station :stationRepository){
            System.out.printf("test");
            if (station.id == id){
                System.out.printf("test");
                retStatio = station;
                break;
            }
        }
        return retStatio;
    }

    private dbStation getStationIdByName(String stationName){
        return  SQLite.select().from(dbStation.class).where(dbStation_Table.stationName.eq(stationName)).queryList().get(0);
    }

    private static boolean contatinsId(List<dbConnectedStation> stations, int id){
        for (dbConnectedStation station: stations){
            if (station.motherStationID == id){
                System.out.printf("test");
                return true;
            }
        }
        return false;
    }

}
