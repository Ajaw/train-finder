package rajwa.com.trainfinder.RailwayModel;

import rajwa.com.trainfinder.Model.*;

import rajwa.com.trainfinder.Model.dbConnectedStation;
import rajwa.com.trainfinder.Model.dbSimpleStation;
import rajwa.com.trainfinder.RailwayModel.structure.SimpleParentStructure;
import android.content.Context;
import android.widget.Toast;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import rajwa.com.trainfinder.Model.dbStation;

/**
 * Created by Dominik on 18.04.2017.
 */

public class SimpleSearch {

    private dbStation startStation;

    private dbStation finishStation;

    private List<dbStation> stationRepository;

    private List<dbSimpleStation> list;

    private Context context;

    private int numberOfSwitches;

    private List<SimpleParentStructure> queue;

    private List<dbSimpleStation> stationOnConnections;
    private HashSet<List<dbSimpleStation>> allRoutes;

    public List<List<dbConnectedStation>> directConnectionSearch(String start, String finish, Context context) {
        try {
            startStation = getStationIdByName(start);
            finishStation = getStationIdByName(finish);
            int startStationId = startStation.id;
            int finishStationId = finishStation.id;
            this.context = context;

            stationRepository = SQLite.select().from(dbStation.class).queryList();
            list = SQLite.select().from(dbSimpleStation.class).queryList();
           // List<ArrivalHour> list2 = SQLite.select().from(ArrivalHour.class).queryList();
           // List<dbConnectionToHour> list3 = SQLite.select().from(dbConnectionToHour.class).queryList();
           // List<dbConnection> list4 = SQLite.select().from(dbConnection.class).queryList();
            //Toast toast = Toast.makeText(context, "Json error" + list1.size(), Toast.LENGTH_LONG);
            //toast.show();

            System.out.printf("test");
        } catch (Exception ex) {

            Toast toast = Toast.makeText(context, "Json error" + ex.getMessage(), Toast.LENGTH_LONG);
            toast.show();
        }
        allRoutes = new HashSet<>();

        stationOnConnections = new LinkedList<>();

        queue = new LinkedList<>();

        List<dbSimpleStation> connectedStations = getStationByID(startStation.id).getSimpleStationConnections();


        queue = new ArrayList<>();
        bfs();


     //   loop(connectedStations, null, null,true);

            retriveDataFromSearch();

        List<List<dbConnectedStation>> connctionsList = findConnectionsByPath();

        System.out.printf("etst");



        return connctionsList;
    }



    private List<List<dbConnectedStation>> findConnectionsByPath(){
        ConnectionSearch search  = new ConnectionSearch(context);
        for (List<dbSimpleStation> route : allRoutes){

            search.findConnections(route);
        }
        return search.allRoutes;
    }

    private void bfs(){
        try {
            List<dbSimpleStation> connectedStations = getStationByID(startStation.id).getSimpleStationConnections();
            SimpleParentStructure p;
            for (dbSimpleStation station : connectedStations) {
                p = new SimpleParentStructure(null, station);
                p.setStructure(null);
                station.structure = p;
            }
            queue = new LinkedList<>();
            LinkedList<dbSimpleStation> nextToVisit = new LinkedList<>();
            HashSet<Integer> visited = new HashSet<>();
            nextToVisit.addAll(connectedStations);
            dbSimpleStation currentStation;
            SimpleParentStructure previousStructer = new SimpleParentStructure(null, null);
            int number_of_switches = 0;
            dbConnectedStation previousStation = new dbConnectedStation();

            while (!nextToVisit.isEmpty()) {

                currentStation = nextToVisit.remove();


                if (currentStation.motherStationID == finishStation.id) {
                    System.out.printf("test");
                    p = new SimpleParentStructure(currentStation.structure.getStation(), currentStation);
                    p.setFinalStation(true);
                    p.setStructure(currentStation.structure);
                    queue.add(p);
                    allRoutes = new HashSet<>();
                    retriveDataFromSearch();
                    return;
                }


                if (visited.contains(currentStation.id)) {
                    continue;
                }

                visited.add(currentStation.id);

                //  previousStation = currentStation;
                List<dbSimpleStation> myConStation = getStationByID(currentStation.connectedStationID).getSimpleStationConnections();

                for (dbSimpleStation station : myConStation) {
                    if (visited.contains(station.id)) {
                        continue;
                    } else {
                            if (currentStation.structure!=null) {
                                p = new SimpleParentStructure(currentStation, station);

                                p.setStructure(currentStation.structure);
                                station.structure = p;
                            }

                        nextToVisit.add(station);
                    }
                }


            }
        }
        catch (Exception ex){
            System.out.printf("etst");
        }

        allRoutes = new HashSet<>();
        retriveDataFromSearch();

        System.out.printf("etst");




    }

    private void loop2(List<dbSimpleStation> connectedStations,List<dbSimpleStation> route, dbSimpleStation previousStation,
                      SimpleParentStructure structure,int current_level,int number_of_switches,boolean first_flag){

//
//        int number_of_iter = connectedStations.size();
//       // int previousNumber = numer_of_switches;
//        try {
//            SimpleParentStructure previousStructer = structure;
//            for (int i = 0; i < number_of_iter; i++) {
//             //   numer_of_switches = previousNumber;
//                if (first_flag) {
//                    previousStructer = null;
//                    previousStation = null;
//                    List<dbSimpleStation> list1 = getStationByID(connectedStations.get(i).connectedStationID).getSimpleStationConnections();
//                    if (list1.size() > 0) {
//                        SimpleParentStructure p = new SimpleParentStructure(previousStation, connectedStations.get(i));
//
//
//                        connectedStations.get(i).visited = true;
//                        loop(list1, previousStation,
//                                 p, false);
//
//                    }
//
//                }
//
//                else  {
//
//
//                   if (connectedStations.get(i).motherStationID == finishStation.id && !(connectedStations.get(i).visited)) {
//                        connectedStations.get(i).visited = true;
//                        stationOnConnections.add(connectedStations.get(i));
//
//                        SimpleParentStructure p = new SimpleParentStructure(previousStation, connectedStations.get(i));
//                        p.setFinalStation(true);
//                        p.setStructure(previousStructer);
//                        queue.add(p);
//                        break;
//
//                    } else {
//                        List<dbSimpleStation> list1 = getStationByID(connectedStations.get(i).connectedStationID).getSimpleStationConnections();
//                        if (list1.size() > 0) {
//                            SimpleParentStructure p = new SimpleParentStructure(previousStation, connectedStations.get(i));
//
//
//                            p.setStructure(previousStructer);
//
//                            connectedStations.get(i).visited = true;
//                            loop(list1, connectedStations.get(i),
//                                   p, false);
//
//                        }
//                        connectedStations.get(i).visited = true;
//                    }
//                    connectedStations.get(i).visited = true;
//                }
//
//            }
//        }
//        catch (Exception ex){
//            Toast toast = Toast.makeText(context,"Json error"+ex.getMessage(),Toast.LENGTH_LONG);
//            toast.show();
//            System.out.printf("etst");
//        }
    }




    private dbStation getStationByID(int id){
        dbStation retStatio = new dbStation();
        for (dbStation station :stationRepository){
            System.out.printf("test");
            if (station.id == id){
                System.out.printf("test");
                retStatio = station;
                break;
            }
        }
        return retStatio;
    }

    private void retriveDataFromSearch(){
        for (SimpleParentStructure structure : queue){
            if (structure.isFinalStation()){
                retraceRoute(structure);
            }
        }

     //   mylist = new HashSet<>(allRoutes);
    }

    private void retraceRoute(SimpleParentStructure p){
        List<dbSimpleStation> route = new ArrayList<>();
        do {
            route.add(p.getStation());

            p = p.getStructure();
        }  while (p.getStructure()!=null);
        route.add(p.getStation());
        allRoutes.add(reversList(route));


        System.out.printf("etst");
    }

    private List<dbSimpleStation> reversList(List<dbSimpleStation> connectedStations){
        Collections.reverse(connectedStations);
        return  connectedStations;
    }


    private dbStation getStationIdByName(String stationName){
        return  SQLite.select().from(dbStation.class).where(dbStation_Table.stationName.eq(stationName)).queryList().get(0);
    }
}
