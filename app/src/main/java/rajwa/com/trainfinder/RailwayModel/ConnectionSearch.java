package rajwa.com.trainfinder.RailwayModel;

import rajwa.com.trainfinder.Model.dbConnectedStation;

import rajwa.com.trainfinder.Model.dbSimpleStation;
import rajwa.com.trainfinder.RailwayModel.structure.ParentStructure;
import android.content.Context;
import android.widget.Toast;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import rajwa.com.trainfinder.Model.dbStation;

/**
 * Created by Dominik on 18.04.2017.
 */

public class ConnectionSearch {

    private List<dbStation> stationRepository;

    private int numberOfSwitches;

    List<ParentStructure> queue;

    private List<dbConnectedStation> stationOnConnections;

    public List<List<dbConnectedStation>> allRoutes;

    private List<dbSimpleStation> route;

    private HashSet<Integer> alreadyAddedConnections;

    private  Context context;

    public ConnectionSearch(Context context){
        stationRepository = SQLite.select().from(dbStation.class).queryList();
        this.context = context;
        alreadyAddedConnections = new HashSet<>();
    }

    public void findConnections(List<dbSimpleStation> route){
        dbSimpleStation start = route.get(0);

        dbStation stratStation = SearchHelper.getStationByID(start.motherStationID,stationRepository);

        this.route = route;
        numberOfSwitches = 0;
        stationOnConnections = new LinkedList<>();
        queue = new LinkedList<>();
        allRoutes = new LinkedList<>();
        for (int i = 0; i < 3; i++) {
            numberOfSwitches = i;
            loop(stratStation.getDirectStationConnection(), null,0,0,null,true);
            if (queue.size()>0){
                retriveDataFromSearch();
                break;
            }
        }




        System.out.printf("test");

    }

    private void loop(List<dbConnectedStation> connectedStations, dbConnectedStation previousStation,
                      int current_level , int numer_of_switches, ParentStructure structure, boolean first_flag){
        //  List<dbStationOnConnection> stationOnConnections = SQLite.select().from(dbStationOnConnection.class).queryList();


        int number_of_iter = connectedStations.size();

        dbSimpleStation currentRouteStation;
        current_level++;
         currentRouteStation = route.get(current_level);
        if (current_level==route.size()-1){
            currentRouteStation=null;
        }

        try {
            ParentStructure previousStructer = structure;
            int previous_number = numer_of_switches;
            for (int i = 0; i < number_of_iter; i++) {
                numer_of_switches = previous_number;

                    if (alreadyAddedConnections.contains(connectedStations.get(i).connectionID)) {
                        continue;
                    }
//                if (first_flag) {
//                    previousStructer = null;
//
//                    previousStation = null;
//                    List<dbConnectedStation> list1 = SearchHelper.getStationByID((connectedStations.get(i).connectedStationID),stationRepository).getDirectStationConnection();
//                    if (list1.size() > 0) {
//                        ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));
//
//                        // queue.add(p);
//
//                        loop(list1, previousStation,current_level,numer_of_switches,
//                                 p, false);
//
//                    }
//                    connectedStations.get(i).visited = true;
//                }
//
//                else  {
                    if (previousStation != null) {
                        if (connectedStations.get(i).connectionID != previousStation.connectionID) {
                            numer_of_switches++;
                        }
                    }

                    if (numer_of_switches >= numberOfSwitches) {

                        connectedStations.get(i).visited = true;
                        continue;
                        // numer_of_switches--;

                    } else if (currentRouteStation == null) {
                        connectedStations.get(i).visited = true;
                        stationOnConnections.add(connectedStations.get(i));
                        alreadyAddedConnections.add(connectedStations.get(i).connectionID);
                        ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));
                        p.setFinalStation(true);
                        p.setStructure(previousStructer);
                        queue.add(p);
                        break;

                    } else if (connectedStations.get(i).connectedStationID == currentRouteStation.motherStationID) {
                        List<dbConnectedStation> list1 = SearchHelper.getStationByID(connectedStations.get(i).connectedStationID, stationRepository).getDirectStationConnection();
                        if (list1.size() > 0) {
                            ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));

//                            if(first_flag) {
//                                p = new ParentStructure(null, connectedStations.get(i));
//                            }
                            p.setStructure(previousStructer);
                            //queue.add(p);
                            // previousStructer = p;
                            loop(list1, connectedStations.get(i), current_level, numer_of_switches
                                    , p, false);
                            connectedStations.get(i).visited = true;
                        }
                        connectedStations.get(i).visited = true;
                    }
                    connectedStations.get(i).visited = true;
                }


           // }
        }
        catch (Exception ex){
            Toast toast = Toast.makeText(context,"Json error"+ex.getMessage(),Toast.LENGTH_LONG);
            toast.show();
            System.out.printf("etst");
        }
    }

    private void retriveDataFromSearch(){
        for (ParentStructure structure : queue){
            if (structure.isFinalStation()){
                retraceRoute(structure);
            }
        }


    }

    private void retraceRoute(ParentStructure p){
        List<dbConnectedStation> route = new ArrayList<>();
        do {
            route.add(p.getStation());

            p = p.getStructure();
        }  while (p.getStructure()!=null);
        route.add(p.getStation());
        allRoutes.add(reversList(route));


        System.out.printf("etst");
    }
    private List<dbConnectedStation> reversList(List<dbConnectedStation> connectedStations){
        Collections.reverse(connectedStations);
        return  connectedStations;
    }
}

