package rajwa.com.trainfinder.RailwayModel.structure;

import rajwa.com.trainfinder.Model.dbConnectedStation;

/**
 * Created by Dominik on 07.04.2017.
 */

public class ParentStructure {

    public ParentStructure(dbConnectedStation parent, dbConnectedStation station){
        parentStation = parent;
        this.station = station;
    }

    public boolean isFinalStation() {
        return finalStation;
    }

    public void setFinalStation(boolean finalStation) {
        this.finalStation = finalStation;
    }

    private boolean finalStation;

    public dbConnectedStation getParentStation() {
        return parentStation;
    }

    private dbConnectedStation parentStation;

    public dbConnectedStation getStation() {
        return station;
    }

    public dbConnectedStation station;

    public ParentStructure getStructure() {
        return structure;
    }

    public void setStructure(ParentStructure structure) {
        this.structure = structure;
    }

    private ParentStructure structure;

}
