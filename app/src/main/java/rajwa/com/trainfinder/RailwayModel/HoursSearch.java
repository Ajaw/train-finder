package rajwa.com.trainfinder.RailwayModel;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import rajwa.com.trainfinder.Model.*;

import rajwa.com.trainfinder.DAOs.dbConnectionDAO;
import rajwa.com.trainfinder.Model.dbConnectedStation;
import rajwa.com.trainfinder.Model.dbConnectionToHour;
import rajwa.com.trainfinder.RailwayModel.structure.HourStructure;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import javax.annotation.Nullable;

import rajwa.com.trainfinder.Model.ArrivalHour;
import rajwa.com.trainfinder.Model.dbStationOnConnection;

/**
 * Created by Dominik on 28.04.2017.
 */

public class HoursSearch {

    public HoursSearch(){
        uniqueConnections = new LinkedList<>();

        dataRepostitory = new LinkedList<>();


    }

    private  List<HourStructure> dataRepostitory;

    private List<List<dbConnectedStation>> uniqueConnections;


    public void trimList(List<dbConnectedStation> stations){
        try {
            int currentConnection = stations.get(0).connectionID;
            int connection_begin = 0;
            int count = 0;
            List<dbConnectedStation> tempStations = new LinkedList<>(stations);
            for (dbConnectedStation station : stations) {
                if (currentConnection != station.connectionID) {

                    tempStations = tempStations.subList(connection_begin, count);
                    dbConnectedStation currentStation = stations.get(tempStations.size()-1);
                    currentConnection = station.connectionID;
                    tempStations.add(SearchHelper.getConnectedStation(currentStation));

                    uniqueConnections.add(tempStations);
                    tempStations = new LinkedList<>(stations);
                    connection_begin = count;
                }


                count++;
            }
            uniqueConnections.add(stations.subList(connection_begin, count));
        }
        catch (Exception ex){
            System.out.printf("test");
        }
    }



    public void findByHour(){

        for (final List<dbConnectedStation> stationList : uniqueConnections){
            HourStructure structure = new HourStructure();
            List<dbConnectionToHour> hourList = findAvalibleConnections(stationList.get(0).connectionID);
            if (hourList.size()==0){
                dataRepostitory = new LinkedList<>();
                return;
            }
            List<dbStationOnConnection> stationsOnConnetionList = dbConnectionDAO.getConnectionById(stationList.get(0).connectionID).getStationList();
             structure = new HourStructure();
            structure.connectionBluePrint = stationList;
            List<List<ArrivalHour>> fullConnectionList = new LinkedList<>();

           int beginIndex =  Iterables.indexOf(stationsOnConnetionList, new Predicate<dbStationOnConnection>() {
                @Override
                public boolean apply(@Nullable dbStationOnConnection input) {
                    if (input.dbStationId == stationList.get(0).motherStationID){
                        return true;
                    }
                    return false;
                }
            });

            int endIndex = Iterables.indexOf(stationsOnConnetionList, new Predicate<dbStationOnConnection>() {
                @Override
                public boolean apply(@Nullable dbStationOnConnection input) {
                    if (input.dbStationId == Iterables.getLast(stationList).motherStationID){
                        return true;
                    }
                    return false;
                }
            });

            for (dbConnectionToHour hour : hourList){


               List<ArrivalHour> arrivalHours =  getArrivalHour(beginIndex, endIndex+1, hour.getConHoursList());
                fullConnectionList.add(arrivalHours);

            }

            structure.arrivalHours = fullConnectionList;
            dataRepostitory.add(structure);
        }

    }

    public List<List<ArrivalHour>> findOptimalConnections(List<dbConnectedStation> stations ){
        try {
            trimList(stations);
            findByHour();
            if (dataRepostitory.size() != 0) {
                HourStructure structure = dataRepostitory.get(0);
                List<List<ArrivalHour>> optimalConnections = new LinkedList<>();
                for (List<ArrivalHour> arrivalHours : structure.arrivalHours) {
                    ArrivalHour currentHour = arrivalHours.get(arrivalHours.size() - 1);
                    if (dataRepostitory.size() > 1) {
                        arrivalHours = hourLoop(currentHour, 1, arrivalHours);
                    }
                    if (arrivalHours.size()>0)
                        optimalConnections.add(arrivalHours);
                }
                System.out.printf("test");
                return optimalConnections;
            }
        }
        catch (Exception ex){
            System.out.printf("test");
            return null;
        }
        List<List<ArrivalHour>> optimalConnections = new LinkedList<>();
        return optimalConnections;
    }

    private List<ArrivalHour>  hourLoop(ArrivalHour hour, int count, List<ArrivalHour> connectionList){
        try {
            HourStructure structure = dataRepostitory.get(count);
            for (List<ArrivalHour> arrivalHours : structure.arrivalHours) {
                if (arrivalHours.get(0).deparetureTime.after(hour.arrivalTime)) {

                    Format formatter = new SimpleDateFormat("HH:mm:ss");
                    String s = formatter.format(arrivalHours.get(0).deparetureTime);
                    Date departure = (Date)formatter.parseObject(s);

                    s = formatter.format(hour.arrivalTime);
                    Date arrival = (Date)formatter.parseObject(s);

                    long diffrence = departure.getTime() - arrival.getTime();

                    System.out.printf("test");
                    if (diffrence < 5400000) {
                        connectionList.addAll(arrivalHours);
                        count++;
                        if (count >= dataRepostitory.size()) {
                            return connectionList;
                        }
                        connectionList = hourLoop(arrivalHours.get(arrivalHours.size()), count, connectionList);
                        return connectionList;
                    }
                }

            }
        }
        catch (NullPointerException ex){
            return connectionList;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  new LinkedList<>();
    }

    public List<ArrivalHour> getArrivalHour(int beginIndex , int endIndex, List<ArrivalHour> hourList){

        return  hourList.subList(beginIndex,endIndex);

    }

    public List<dbConnectionToHour> findAvalibleConnections(int connectionID ){
        return  SQLite.select().from(dbConnectionToHour.class).where(dbConnectionToHour_Table.dbConnectionId.eq(connectionID)).queryList();
        }


    public void findConnections(int beginStation, int EndStation, List<dbConnectionToHour> hoursList){

    }
}
