package rajwa.com.trainfinder.Model.precomputedData;

import rajwa.com.trainfinder.Database.TrainDB;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;


/**
 * Created by Dominik on 09.05.2017.
 */
@Table(database = TrainDB.class,cachingEnabled = true)
public class dbPrecomputedSimpleStop extends BaseModel implements Serializable {



    @PrimaryKey
    @Column
    @SerializedName("id")
    private int id;
    @Column
    @SerializedName("station_ID")
    private int station_ID;
    @Column
    private int partID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStation_ID() {
        return station_ID;
    }

    public void setStation_ID(int station_ID) {
        this.station_ID = station_ID;
    }

    public int getPartID() {
        return partID;
    }

    public void setPartID(int partID) {
        this.partID = partID;
    }
}
