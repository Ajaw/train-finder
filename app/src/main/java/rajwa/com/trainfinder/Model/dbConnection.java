package rajwa.com.trainfinder.Model;

import rajwa.com.trainfinder.Database.TrainDB;

import rajwa.com.trainfinder.Model.dbConnectionToHour_Table;
import rajwa.com.trainfinder.Model.dbStationOnConnection_Table;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

/**
 * Created by Dominik on 16.03.2017.
 */

@Table(database = TrainDB.class)
public class dbConnection extends BaseModel {

    public int getId() {
        return id;
    }

    @Column
    @PrimaryKey
    @SerializedName("id")
    int id;

    @Column
    boolean wasPrecomputed;

    List<dbConnectionToHour> hoursList;

    List<dbStationOnConnection> stationsList;

    @SerializedName("hoursList")
    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "hoursList")
    public List<dbConnectionToHour> getHoursList(){
        if (hoursList==null || hoursList.isEmpty()){
            hoursList = SQLite.select().from(dbConnectionToHour.class).where(dbConnectionToHour_Table.dbConnectionId.eq(this.id)).queryList();
        }

        return hoursList;
    }

    @SerializedName("stationsList")
    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "stationsList")
    public List<dbStationOnConnection> getStationList(){
        if (stationsList==null || stationsList.isEmpty()){
            stationsList = SQLite.select().from(dbStationOnConnection.class).where(dbStationOnConnection_Table.dbConnectionId.eq(this.id)).queryList();
        }

        return stationsList;
    }


}
