package rajwa.com.trainfinder.Model;

import rajwa.com.trainfinder.Database.TrainDB;

import rajwa.com.trainfinder.Model.dbConnectedStation_Table;
import rajwa.com.trainfinder.Model.dbSimpleStation_Table;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

/**
 * Created by Dominik on 16.03.2017.
 */
@Table(database = TrainDB.class)
public class dbStation extends BaseModel {

    @SerializedName("id")
    @Column
    @PrimaryKey
    public int id;


    @SerializedName("stationName")
    @Column
    public String stationName;

    @SerializedName("x")
    @Column
    public double X;

    public double getX() {
        return X;
    }

    public void setX(double x) {
        X = x;
    }

    public double getY() {
        return Y;
    }

    public void setY(double y) {
        Y = y;
    }

    @SerializedName("y")
    @Column
    public double Y;

    List<dbConnectedStation> directStationConnection;

    List<dbSimpleStation> simpleStationConnections;


    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "directStationConnection")
    public List<dbConnectedStation> getDirectStationConnection(){
        if (directStationConnection==null || directStationConnection.isEmpty()){
            directStationConnection = SQLite.select().from(dbConnectedStation.class)
                    .where(dbConnectedStation_Table.motherStationID.eq(id)).queryList();
        }
        return directStationConnection;
    }


    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "simpleStationConnections")
    public List<dbSimpleStation> getSimpleStationConnections(){
        if (simpleStationConnections==null || simpleStationConnections.isEmpty()){
            simpleStationConnections = SQLite.select().from(dbSimpleStation.class)
                    .where(dbSimpleStation_Table.motherStationID.eq(id)).queryList();
        }
        return simpleStationConnections;
    }
//


//    List<dbConnectionToHour> hourList;

//    @OneToMany(methods = {OneToMany.Method.ALL}, _variableName ="hourList")
//    public  List<dbConnectionToHour> getHourList(){
//
//    }


}
