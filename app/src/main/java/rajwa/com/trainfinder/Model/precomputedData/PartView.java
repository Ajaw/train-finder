package rajwa.com.trainfinder.Model.precomputedData;

import rajwa.com.trainfinder.Database.TrainDB;

import rajwa.com.trainfinder.Model.precomputedData.dbPrecomputedConnection_Table;
import rajwa.com.trainfinder.Model.precomputedData.dbPrecomputedPart_Table;
import rajwa.com.trainfinder.Model.precomputedData.dbPrecomputedSimpleStop_Table;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelView;
import com.raizlabs.android.dbflow.annotation.ModelViewQuery;
import com.raizlabs.android.dbflow.sql.Query;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dominik on 18.05.2017.
 */
@ModelView(database = TrainDB.class)
public class PartView implements Serializable {

    @ModelViewQuery
    public  static final Query QUERY = SQLite.select(dbPrecomputedPart_Table.id).from(dbPrecomputedPart.class);

    @Column
    int id;

    public List<dbPrecomputedSimpleStop> getStops() {
        if (stops == null) {
            stops = SQLite.select().from(dbPrecomputedSimpleStop.class).where(dbPrecomputedSimpleStop_Table.partID.eq(id)).orderBy(dbPrecomputedSimpleStop_Table.id,true).queryList();
        }
        return stops;
    }

    public List<dbPrecomputedConnection> getConnections() {
        if (connections == null) {
            connections = SQLite.select().from(dbPrecomputedConnection.class).where(dbPrecomputedConnection_Table.partID.eq(id)).queryList();
        }
        return connections;
    }

    public void setStops(List<dbPrecomputedSimpleStop> stops) {
        this.stops = stops;
    }

    public void setConnections(List<dbPrecomputedConnection> connections) {
        this.connections = connections;
    }

    private List<dbPrecomputedSimpleStop> stops;
    private List<dbPrecomputedConnection> connections;

    public int startIndex;
    public int finishIndex;

}
