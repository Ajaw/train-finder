package rajwa.com.trainfinder.Model;

import rajwa.com.trainfinder.Database.TrainDB;
import rajwa.com.trainfinder.RailwayModel.structure.SimpleParentStructure;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Dominik on 17.04.2017.
 */

@Table(database = TrainDB.class)
public class dbSimpleStation extends BaseModel{


    @Column
    @PrimaryKey
    public int id;

    @Column
    public int connectedStationID;

    @Column
    public int motherStationID;

    @Column
    public int previousStationID;

    public  boolean visited;

    public SimpleParentStructure structure;

}

