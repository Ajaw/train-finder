package rajwa.com.trainfinder.Model;

import rajwa.com.trainfinder.Database.TrainDB;
import rajwa.com.trainfinder.RailwayModel.structure.ParentStructure;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Index;
import com.raizlabs.android.dbflow.annotation.IndexGroup;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Dominik on 01.04.2017.
 */

@Table(database = TrainDB.class,
        indexGroups = @IndexGroup(number = 1,name = "firstIndex"))

public class dbConnectedStation extends BaseModel {

    @Index(indexGroups = 1)
    @Column
    @PrimaryKey
    public int id;

    @Column
    public int motherStationID;

    @Column
    public int connectedStationID;

    @Column
    public int previousStationID;

    @Column
    public int connectionID;

    @Column
    public boolean finalStation;

    public  boolean visited;

    public int number_of_switches;

    public ParentStructure structure;

}
