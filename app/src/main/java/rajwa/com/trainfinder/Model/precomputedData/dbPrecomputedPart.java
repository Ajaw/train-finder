package rajwa.com.trainfinder.Model.precomputedData;

import rajwa.com.trainfinder.Database.TrainDB;

import rajwa.com.trainfinder.Model.precomputedData.dbPrecomputedConnection_Table;
import rajwa.com.trainfinder.Model.precomputedData.dbPrecomputedSimpleStop_Table;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;


import java.util.List;

/**
 * Created by Dominik on 09.05.2017.
 */
@Table(database = TrainDB.class,cachingEnabled = true)
public class dbPrecomputedPart extends BaseModel {


    @PrimaryKey
    @Column
    @SerializedName("id")
    private int id;



    @SerializedName("connections")
     List<dbPrecomputedConnection> connections;
    @SerializedName("stopsOnStation")
     List<dbPrecomputedSimpleStop> stopsOnStation;

    public int startIndex;
    public int finishIndex;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @OneToMany(methods = {OneToMany.Method.LOAD}, variableName = "connections")
    public List<dbPrecomputedConnection> getConnections() {
        if (connections==null || connections.isEmpty()){
            connections = SQLite.select().from(dbPrecomputedConnection.class).where(dbPrecomputedConnection_Table.partID.eq(this.id)).queryList();
        }

        return connections;
    }

    public void setConnections(List<dbPrecomputedConnection> connections) {
        this.connections = connections;
    }


    @OneToMany(methods = {OneToMany.Method.LOAD}, variableName = "stopsOnStation")
    public List<dbPrecomputedSimpleStop> getStopsOnStation() {
        if (stopsOnStation==null || stopsOnStation.isEmpty()){
            stopsOnStation = SQLite.select().from(dbPrecomputedSimpleStop.class).where(dbPrecomputedSimpleStop_Table.partID.eq(this.id)).queryList();
        }

        return stopsOnStation;
    }

    public void setStopsOnStation(List<dbPrecomputedSimpleStop> stopsOnStation) {
        this.stopsOnStation = stopsOnStation;
    }

}
