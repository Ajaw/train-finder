package rajwa.com.trainfinder.Model.precomputedData;

import rajwa.com.trainfinder.Database.TrainDB;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;


/**
 * Created by Dominik on 09.05.2017.
 */
@Table(database = TrainDB.class)
public class dbPrecomputedStop extends BaseModel {

    @Column
    @PrimaryKey
    @SerializedName("id")
    private int id;
    @Column
    @SerializedName("connectedStationID")
    private int connectedStationID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getConnectedStationID() {
        return connectedStationID;
    }

    public void setConnectedStationID(int connectedStationID) {
        this.connectedStationID = connectedStationID;
    }

    public int getPrecomputedConnectionID() {
        return precomputedConnectionID;
    }

    public void setPrecomputedConnectionID(int precomputedConnectionID) {
        this.precomputedConnectionID = precomputedConnectionID;
    }

    @Column
    private int precomputedConnectionID;


}
