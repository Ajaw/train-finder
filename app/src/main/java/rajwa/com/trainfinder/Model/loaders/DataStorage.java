package rajwa.com.trainfinder.Model.loaders;

import rajwa.com.trainfinder.Model.precomputedData.PartView;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dominik on 18.05.2017.
 */

public class DataStorage implements Serializable {

    public DataStorage(List<PartView> objects) {
        this.objects = objects;
    }

    public List<PartView> getObjects() {
        return objects;
    }

    public void setObjects(List<PartView> objects) {
        this.objects = objects;
    }

    public List<PartView> objects;

}
