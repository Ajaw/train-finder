package rajwa.com.trainfinder.Model;

import rajwa.com.trainfinder.Database.TrainDB;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.sql.Date;

/**
 * Created by Dominik on 16.03.2017.
 */

@Table(database = TrainDB.class)
public class ArrivalHour extends BaseModel {

    @Column
    @PrimaryKey
    @SerializedName("id")
    public int hour_id;

    @Column
    @SerializedName("connectionToHourId")
    public int connectionToHourId;

    @SerializedName("stationOnConnectionId")
    @Column
    public int stationID;

    @Column
    @SerializedName("arrivalTime")
    public Date arrivalTime;

    @Column
    @SerializedName("departureTime")
    public Date deparetureTime;

    @Column
    @SerializedName("platform")
    public String platform;

    @Column
    @SerializedName("track")
    public String track;

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    private Date currentDate;

    public int getId() {
        return hour_id;
    }

    public void setId(int id) {
        this.hour_id = id;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Date getDeparetureTime() {
        return deparetureTime;
    }

    public void setDeparetureTime(Date deparetureTime) {
        this.deparetureTime = deparetureTime;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public int getStationID() {
        return stationID;
    }

    public void setStationID(int stationID) {
        this.stationID = stationID;
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

}
