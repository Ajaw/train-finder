package rajwa.com.trainfinder.Model;

import rajwa.com.trainfinder.Database.TrainDB;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.sql.Date;

/**
 * Created by Dominik on 16.11.2017.
 */
@Table(database = TrainDB.class)
public class dbConnectionAvalabities extends BaseModel {

    @Column
    @PrimaryKey
    @SerializedName("id")
    private int id;
    @Column
    @SerializedName("beggingDate")
    private Date beggingDate;
    @Column
    @SerializedName("endDate")
    private Date endDate;
    @Column
    @SerializedName("ignoreWeekDays")
    private boolean ignoreWeekDays;
    @Column
    @SerializedName("mapping_id")
    private Integer mapping_id;
    private dbConnectionMapping mapping;
    @Column
    private Integer versionId;

    public Integer getMapping_id() {
        return mapping_id;
    }

    public void setMapping_id(Integer mapping_id) {
        this.mapping_id = mapping_id;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public boolean isIgnoreWeekDays() {
        return ignoreWeekDays;
    }

    public void setIgnoreWeekDays(boolean ignoreWeekDays) {
        this.ignoreWeekDays = ignoreWeekDays;
    }

    public dbConnectionMapping getMapping() {
        return mapping;
    }

    public void setMapping(dbConnectionMapping mapping) {
        this.mapping = mapping;
    }


    public Date getBeggingDate() {
        return beggingDate;
    }

    public void setBeggingDate(Date beggingDate) {
        this.beggingDate = beggingDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    public Integer getVersionId() {
        return versionId;
    }

    public void setVersionId(Integer versionId) {
        this.versionId = versionId;
    }





}
