package rajwa.com.trainfinder.Model.precomputedData;

import rajwa.com.trainfinder.Database.TrainDB;

import rajwa.com.trainfinder.Model.precomputedData.dbPrecomputedStop_Table;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

/**
 * Created by Dominik on 09.05.2017.
 */
@Table(database = TrainDB.class)
public class dbPrecomputedConnection extends BaseModel {


    @PrimaryKey
    @Column
    @SerializedName("id")
    private int id;



    @PrimaryKey
    @Column
    private int partID;

    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "precomputedStops")
    public List<dbPrecomputedStop> getPrecomputedStops() {
        if (precomputedStops==null || precomputedStops.isEmpty()){
            precomputedStops = SQLite.select().from(dbPrecomputedStop.class).where(dbPrecomputedStop_Table.precomputedConnectionID.eq(this.id)).queryList();
        }

        return precomputedStops;
    }

    public void setPrecomputedStops(List<dbPrecomputedStop> precomputedStops) {
        this.precomputedStops = precomputedStops;
    }

    @SerializedName("precomputedStops")
    List<dbPrecomputedStop> precomputedStops;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPartID() {
        return partID;
    }

    public void setPartID(int partID) {
        this.partID = partID;
    }

}
