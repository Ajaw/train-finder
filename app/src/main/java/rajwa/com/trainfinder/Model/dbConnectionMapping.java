package rajwa.com.trainfinder.Model;

import rajwa.com.trainfinder.Database.TrainDB;

import rajwa.com.trainfinder.Model.dbConnectionAvalabities_Table;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

/**
 * Created by Dominik on 16.11.2017.
 */
@Table(database = TrainDB.class)
public class dbConnectionMapping extends BaseModel {

    @Column
    @PrimaryKey
    @SerializedName("id")
    private int id;
    @Column
    @SerializedName("monday")
    private boolean monday;
    @Column
    @SerializedName("tuesday")
    private boolean tuesday;
    @Column
    @SerializedName("wenesday")
    private boolean wenesday;
    @Column
    @SerializedName("thursday")
    private boolean thursday;
    @Column
    @SerializedName("friday")
    private boolean friday;
    @Column
    @SerializedName("saturday")
    private boolean saturday;
    @Column
    @SerializedName("sunday")
    private boolean sunday;
    @Column
    @SerializedName("connectionNumber")
    private String connectionNumber;
    @Column
    @SerializedName("connectionId")
    private Integer connectionId;
    @Column
    @SerializedName("connectionToHourId")
    private Integer connectionToHourId;
    @Column
    private Integer versionId;

    List<dbConnectionAvalabities> availabilities;


    @OneToMany( methods = {OneToMany.Method.ALL}, variableName = "availabilities")
    @SerializedName("availabilities")
    public List<dbConnectionAvalabities> getAvailabilities(){
        if (availabilities==null || availabilities.isEmpty()){

            availabilities = SQLite.select().from(dbConnectionAvalabities.class).where(dbConnectionAvalabities_Table.mapping_id.eq(this.id)).queryList();
        }

        return availabilities;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isMonday() {
        return monday;
    }

    public void setMonday(boolean monday) {
        this.monday = monday;
    }

    public boolean isTuesday() {
        return tuesday;
    }

    public void setTuesday(boolean tuesday) {
        this.tuesday = tuesday;
    }

    public boolean isWenesday() {
        return wenesday;
    }

    public void setWenesday(boolean wenesday) {
        this.wenesday = wenesday;
    }

    public boolean isThursday() {
        return thursday;
    }

    public void setThursday(boolean thursday) {
        this.thursday = thursday;
    }

    public boolean isFriday() {
        return friday;
    }

    public void setFriday(boolean friday) {
        this.friday = friday;
    }

    public boolean isSaturday() {
        return saturday;
    }

    public void setSaturday(boolean saturday) {
        this.saturday = saturday;
    }

    public boolean isSunday() {
        return sunday;
    }

    public void setSunday(boolean sunday) {
        this.sunday = sunday;
    }

    public String getConnectionNumber() {
        return connectionNumber;
    }

    public void setConnectionNumber(String connectionNumber) {
        this.connectionNumber = connectionNumber;
    }

    public Integer getVersionId() {
        return versionId;
    }

    public void setVersionId(Integer versionId) {
        this.versionId = versionId;
    }

    public Integer getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Integer connectionId) {
        this.connectionId = connectionId;
    }

    public Integer getConnectionToHourId() {
        return connectionToHourId;
    }

    public void setConnectionToHourId(Integer connectionToHourId) {
        this.connectionToHourId = connectionToHourId;
    }




}
