package rajwa.com.trainfinder.Model;

import rajwa.com.trainfinder.Database.TrainDB;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Dominik on 16.03.2017.
 */
@Table(database = TrainDB.class)
public class dbStationOnConnection  extends BaseModel{


    @Column
    @PrimaryKey
    @SerializedName("id")
    public int id;

    @Column
    @SerializedName("connectionId")
    public int dbConnectionId;


    @Column
    @SerializedName("stationId")
    public int dbStationId;



    @Column
    @SerializedName("stationNumberOnRoute")
    public int stationNumberOnRoute;


    @Column
    @SerializedName("distanceFromLastStation")
    public float distanceFromLastStation;




}
