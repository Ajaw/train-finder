package rajwa.com.trainfinder.Model;

import rajwa.com.trainfinder.Database.TrainDB;

import rajwa.com.trainfinder.Model.PrecomputedStation_Table;
import rajwa.com.trainfinder.Model.dbConnectedStation_Table;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dominik on 14.04.2017.
 */
@Table(database = TrainDB.class)
public class PrecomputedPath extends BaseModel {

    public void setId(int id) {
        this.id = id;
    }

    @Column
    @PrimaryKey (autoincrement=true)
    @SerializedName("id")
    int id;

//    @Column
//    int numberOfSwitches;


    public void setStationsList(List<dbConnectedStation> stationsList) {
        this.stationsList = stationsList;
    }

    List<dbConnectedStation> stationsList;




    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "stationsList")
    public List<dbConnectedStation> getStationList(){


        if (stationsList==null || stationsList.isEmpty()){
            stationsList = new LinkedList<>();
            List<PrecomputedStation> stations =SQLite.select().from(PrecomputedStation.class).where(PrecomputedStation_Table.precomputedPathID.eq(this.id)).queryList();

            for (PrecomputedStation station :stations){
                stationsList.add(SQLite.select().from(dbConnectedStation.class).where(dbConnectedStation_Table.id.eq(station.connectedStationID)).querySingle());
            }
        }

        return stationsList;
    }

}
