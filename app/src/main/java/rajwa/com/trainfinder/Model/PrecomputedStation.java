package rajwa.com.trainfinder.Model;

import rajwa.com.trainfinder.Database.TrainDB;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Dominik on 14.04.2017.
 */
@Table(database = TrainDB.class)
public class PrecomputedStation extends BaseModel{

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrecomputedPathID() {
        return precomputedPathID;
    }

    public void setPrecomputedPathID(int precomputedPathID) {
        this.precomputedPathID = precomputedPathID;
    }

    public int getConnectedStationID() {
        return connectedStationID;
    }

    public void setConnectedStationID(int connectedStationID) {
        this.connectedStationID = connectedStationID;
    }

    @Column
    @PrimaryKey
    int id;

    @Column
    int precomputedPathID;

    @Column
    int connectedStationID;






}
