package rajwa.com.trainfinder.Model.loaders;

import java.util.Date;
import java.util.List;

import rajwa.com.trainfinder.Model.ArrivalHour;
import rajwa.com.trainfinder.Model.dbConnection;

/**
 * Created by Dominik on 22.11.2017.
 */

public class SearchData {

    private dbConnection connection;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    private Date date;

    public dbConnection getConnection() {
        return connection;
    }

    public void setConnection(dbConnection connection) {
        this.connection = connection;
    }

    public List<ArrivalHour> getArrivalHours() {
        return arrivalHours;
    }

    public void setArrivalHours(List<ArrivalHour> arrivalHours) {
        this.arrivalHours = arrivalHours;
    }

    private List<ArrivalHour> arrivalHours;


}
