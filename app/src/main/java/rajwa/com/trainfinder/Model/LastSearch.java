package rajwa.com.trainfinder.Model;

import rajwa.com.trainfinder.Database.TrainDB;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;

import java.util.Date;

/**
 * Created by Dominik on 27.12.2017.
 */
@Table(database = TrainDB.class)
public class LastSearch {
    @Column
    @PrimaryKey(autoincrement=true)
    @SerializedName("id")
    private int id;

    public Date getSearchedDate() {
        return searchedDate;
    }

    public void setSearchedDate(Date searchedDate) {
        this.searchedDate = searchedDate;
    }

    public Date getInsertedDate() {
        return insertedDate;
    }

    public void setInsertedDate(Date insertedDate) {
        this.insertedDate = insertedDate;
    }

    @Column
    private Date searchedDate;

    private Date insertedDate;


    @Column
    private String stationFrom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getStationFrom() {
        return stationFrom;
    }

    public void setStationFrom(String stationFrom) {
        this.stationFrom = stationFrom;
    }

    public String getStationTo() {
        return stationTo;
    }

    public void setStationTo(String stationTo) {
        this.stationTo = stationTo;
    }

    @Column
    private String stationTo;

}
