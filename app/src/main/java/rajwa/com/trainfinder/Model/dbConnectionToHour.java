package rajwa.com.trainfinder.Model;

import rajwa.com.trainfinder.Database.TrainDB;

import rajwa.com.trainfinder.Model.ArrivalHour_Table;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.ForeignKeyReference;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import  com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Dominik on 16.03.2017.
 */
@Table(database = TrainDB.class)

public class dbConnectionToHour extends BaseModel {

    @Column
    @PrimaryKey
    @SerializedName("id")
    public int id;

//    @Column
//    @ForeignKey(saveForeignKeyModel = false)
//    public dbConnection connection;

    @Column
    @ForeignKey(tableClass = dbConnection.class, references = {@ForeignKeyReference(columnName = "dbConnectionId",foreignKeyColumnName = "id")})
    @SerializedName("connectionId")
    public int dbConnectionId;

    @Column
    @SerializedName("onWeekendConnection")
    public boolean onWeekendConnetion;


    List<ArrivalHour> conHoursList;


    @OneToMany( methods = {OneToMany.Method.ALL}, variableName = "conHoursList")
    @SerializedName("conHoursList")
    public List<ArrivalHour> getConHoursList(){
        if (conHoursList==null || conHoursList.isEmpty()){
          return   conHoursList = SQLite.select().from(ArrivalHour.class).where(ArrivalHour_Table.connectionToHourId.eq(this.id)).orderBy(ArrivalHour_Table.hour_id,true).queryList();
        }

        return conHoursList;
    }

}




