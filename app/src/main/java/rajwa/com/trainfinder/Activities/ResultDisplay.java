package rajwa.com.trainfinder.Activities;

import rajwa.com.trainfinder.Database.TrainDB;
import rajwa.com.trainfinder.DatabaseSearch.SimpleConnectionSearch;
import rajwa.com.trainfinder.Model.loaders.DataStorage;
import rajwa.com.trainfinder.Model.loaders.SearchData;
import rajwa.com.trainfinder.RailwayModel.SearchForConnection;
import rajwa.com.trainfinder.RecyclerViews.Adapters.Adapter;
import rajwa.com.trainfinder.RecyclerViews.Adapters.OnLoadMoreListener;
import rajwa.com.trainfinder.RecyclerViews.Model.SearchParent;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import rajwa.com.trainfinder.R;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapterHelper;
import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import com.google.common.base.Function;
import com.google.common.collect.Ordering;

import org.joda.time.DateTime;

import java.io.File;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Nullable;

import rajwa.com.trainfinder.Model.ArrivalHour;
import rajwa.com.trainfinder.Model.dbConnectedStation;

public class ResultDisplay extends AppCompatActivity {


    RecyclerView recyclerView;

    protected Handler handler;

    private String start;
    private String end;
    private DataStorage storage;
    private Date choosenDate;
    public    List<ParentObject> objects;
    public List<Object> objectData;
    private Adapter adapter;
    private boolean firstRun;
    private boolean shouldLoop;
    private Integer count;
    private boolean launchSearch;
    private boolean addSpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        firstRun = true;
        shouldLoop = true;
        count = 0;
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_result_display);
        objectData = new LinkedList<>();
        handler = new Handler();

        recyclerView = (RecyclerView)findViewById(R.id.searchResultView);

       // findViewById(R.id.loading_search_progress).setVisibility(View.VISIBLE);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

       objects = new LinkedList<>();


        Bundle bundle = getIntent().getExtras();

         storage = (DataStorage)bundle.getSerializable("partData");

         start = getIntent().getStringExtra("startStation");
         end = getIntent().getStringExtra("endStation");

         choosenDate = new Date();
        try {
            choosenDate = new Date(Long.parseLong(getIntent().getStringExtra("currentDate")));
        }
        catch (Exception ex){
            choosenDate = new Date();
        }
        loadData();

        adapter.resultDisplay = this;

        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                    new Thread(new Runnable() {

                    public void run() {
                        shouldLoop = true;
                        addSpinner = true;
                        launchSearch = false;
                        if (firstRun) {
                             addSpinner = false;
                            launchSearch = true;
                        }
                        if (count>=5){
                            adapter.setLoaded();
                        }

                        while (shouldLoop && count<5) {
                            if (addSpinner && !launchSearch) {

                                //objectData.clear();
                                Log.e("SIZE INSERT", String.valueOf(objectData.size()));
                               // objectData.addAll(new LinkedList<Object>(adapter.mItemList));
                                objectData.add(null);
                                objects.add(null);
                                Log.e("SIZE INSERT", String.valueOf(objectData.size()));
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //   findViewById(R.id.loading_search_progress).setVisibility(View.VISIBLE);
                                        Log.e("NOTIFY SIZE", String.valueOf(objectData.size()));
                                       // adapter.mExpandableRecyclerAdapterHelper = new ExpandableRecyclerAdapterHelper(objectData);
                                        adapter.notifyDataSetChanged();
                                        recyclerView.getRecycledViewPool().clear();
                                        launchSearch = true;
                                    }
                                });
                                addSpinner = false;

                            }
                            if (launchSearch) {
                                if (!firstRun) {
                                    DateTime dateTime = new DateTime(choosenDate);


                                    dateTime = dateTime.plusDays(1);
                                    dateTime = dateTime.withHourOfDay(0);
                                    dateTime = dateTime.withMinuteOfHour(0);
                                    choosenDate = dateTime.toDate();
                                }

                                SimpleConnectionSearch search1 = new SimpleConnectionSearch();

                                List<List<ArrivalHour>> connections = search1.getConnection(start, end, storage.getObjects(), choosenDate);
                                // List<dbConnectedStation> connectionList = connections.get(0);
                                List<dbConnectedStation> connectionList = new LinkedList<>();
                                System.out.printf("test");
                                if (connectionList != null) {
                                    List<List<ArrivalHour>> optmialHours = connections;
//                for (List<dbConnectedStation> stationList : connections) {
//                    HoursSearch hoursSearch = new HoursSearch();
//                    optmialHours.addAll(hoursSearch.findOptimalConnections(stationList));
//                }
                                    Ordering<SearchData> ordering = Ordering.natural().onResultOf(new Function<SearchData, Comparable>() {
                                        @Nullable
                                        @Override
                                        public Comparable apply(@Nullable SearchData input) {
                                            Format formatter = new SimpleDateFormat("HHmm");
                                            String s = formatter.format(input.getArrivalHours().get(0).deparetureTime);
                                            return Integer.parseInt(s);
                                        }
                                    });
                                    Collections.sort(search1.getSearchData(), ordering);
                                    if (search1.getSearchData().size() != 0) {
                                        if (search1.getSearchData().size()>1) {
                                            shouldLoop = false;
                                        }
                                        else {
                                            launchSearch = true;
                                            addSpinner = false;
                                        }


                                        if (objects.size()!=1 || (objects.get(0)==null && objects.size()==1)) {
                                            Log.e("SIZE REMOVE", String.valueOf(objects.size()));
                                            Log.e("Adapter size", String.valueOf(adapter.mItemList.size()));
                                            objects.remove(objects.size() - 1);
                                            objectData.remove(objectData.size() - 1);
                                            Log.e("Adapter size", String.valueOf(adapter.mItemList.size()));
                                            //objectData.clear();
                                            //objectData.addAll(new LinkedList<Object>(adapter.mItemList));
                                            Log.e("Adapter size", String.valueOf(adapter.mItemList.size()));
                                            //objectData.addAll(new LinkedList<Object>(objects));

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Log.e("NOTIFY REMOVE SIZE", String.valueOf(objectData.size()));
                                                    adapter.notifyDataSetChanged();


                                                    //adapter.notifyItemRemoved(objects.size());
                                                }

                                            });
                                        }
                                        SystemClock.sleep(250);
                                        objects.addAll(initData(search1.getSearchData()));
                                        objectData.addAll(initData(search1.getSearchData()));
                                        //objectData.clear();
                                        Log.e("Objects size", String.valueOf(objects.size()));
                                        Log.e("Adapter size", String.valueOf(adapter.mItemList.size()));

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                adapter.mExpandableRecyclerAdapterHelper = new ExpandableRecyclerAdapterHelper(adapter.mItemList);

                                                adapter.setLoaded();
                                                adapter.notifyDataSetChanged();
                                                recyclerView.getRecycledViewPool().clear();
                                                launchSearch = false;
                                                addSpinner = true;
                                            }
                                        });



                                    }
                                }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //  findViewById(R.id.loading_search_progress).setVisibility(View.GONE);
                                    }
                                });

                                count++;
                                firstRun = false;
                            }
                        }
                    }
                }).start();
            }
        });



        SearchForConnection search = new SearchForConnection();





        File f = getApplicationContext().getDatabasePath(TrainDB.NAME);

        long dbSize = f.length();

       // search.directConnectionSearch(start,end,getApplicationContext());

        //SimpleSearch search2 = new SimpleSearch();
       // List<List<dbConnectedStation>> connectionList = search2.directConnectionSearch(start,end,getApplicationContext());


    }

    private void loadData(){
        try {

            SimpleConnectionSearch search1 = new SimpleConnectionSearch();
           // List<List<ArrivalHour>> connections = search1.getConnection(start,end,storage.getObjects(),choosenDate);
            // List<dbConnectedStation> connectionList = connections.get(0);
            List<dbConnectedStation> connectionList = new LinkedList<>();
            System.out.printf("test");
            if (connectionList != null) {
//                List<List<ArrivalHour>> optmialHours = connections;
////                for (List<dbConnectedStation> stationList : connections) {
////                    HoursSearch hoursSearch = new HoursSearch();
////                    optmialHours.addAll(hoursSearch.findOptimalConnections(stationList));
////                }
//                Ordering<SearchData> ordering = Ordering.natural().onResultOf(new Function<SearchData, Comparable>() {
//                    @Nullable
//                    @Override
//                    public Comparable apply(@Nullable SearchData input) {
//                        Format formatter = new SimpleDateFormat("HHmm");
//                        String s = formatter.format(input.getArrivalHours().get(0).deparetureTime);
//                        return Integer.parseInt(s);
//                    }
//                });
//                Collections.sort(search1.getSearchData(), ordering);
//                if (search1.getSearchData().size() != 0) {
//                    objects.addAll(initData(search1.getSearchData()));
//                    objectData.clear();
//                    objectData.addAll(new LinkedList<Object>(objects));
//                    adapter = new Adapter(this, objects,objectData,recyclerView);
//                    //Adapter adapter = new Adapter(this, initData(search1.getSearchData()));
//
//                    adapter.setParentClickableViewAnimationDefaultDuration();
//                    adapter.setParentAndIconExpandOnClick(true);
//                    adapter.notifyDataSetChanged();
//                    recyclerView.setAdapter(adapter);
//                }
//                else {

                    objects = new LinkedList<>();
                    objects.add(null);
                    objectData = new LinkedList<Object>(objects);
                    adapter = new Adapter(this, objects,objectData,recyclerView);
                    //Adapter adapter = new Adapter(this, initData(search1.getSearchData()));

                    adapter.setParentClickableViewAnimationDefaultDuration();
                    adapter.setParentAndIconExpandOnClick(true);
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);

               // }
            }
           // findViewById(R.id.loading_search_progress).setVisibility(View.INVISIBLE);
        }
        catch (Exception ex){
            System.out.printf("test");
        }
    }

    private List<ParentObject> initData(List<SearchData> optimalHours){

        List<SearchParent> searchResults = new LinkedList<>();

        List<ParentObject> parentObjects = new ArrayList<>();
        for (SearchData result : optimalHours){
            SearchParent parent = new SearchParent();
            parent.setDate(result.getDate());
            parent.setConnection(result.getConnection());
            List<Object> childList = new ArrayList<>();
            childList.addAll(result.getArrivalHours());
            parent.setChildObjectList(childList);
            parentObjects.add(parent);
        }

        return parentObjects;

    }
}
