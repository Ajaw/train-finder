package rajwa.com.trainfinder.Activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import rajwa.com.trainfinder.DAOs.dbScheduleVersionDAO;

import rajwa.com.trainfinder.R;

import rajwa.com.trainfinder.Model.dbConnectionToHour;
import rajwa.com.trainfinder.Model.dbScheduleVersion;
import rajwa.com.trainfinder.Model.precomputedData.dbPrecomputedConnection;
import rajwa.com.trainfinder.Model.precomputedData.dbPrecomputedPart;
import rajwa.com.trainfinder.Model.precomputedData.dbPrecomputedStop;
import rajwa.com.trainfinder.SearchScreen;
import rajwa.com.trainfinder.TrainFinderApplication;
import rajwa.com.trainfinder.UpdateData.DataCallback;
import rajwa.com.trainfinder.UpdateData.stationUpdater;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Path;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.animation.PathInterpolatorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.ModelAdapter;
import com.raizlabs.android.dbflow.structure.database.transaction.FastStoreModelTransaction;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import rajwa.com.trainfinder.Model.ArrivalHour;
import rajwa.com.trainfinder.Model.dbConnection;
import rajwa.com.trainfinder.Model.dbConnectionAvalabities;
import rajwa.com.trainfinder.Model.dbConnectionMapping;
import rajwa.com.trainfinder.Model.dbStation;
import rajwa.com.trainfinder.Model.dbStationOnConnection;
import rajwa.com.trainfinder.Model.precomputedData.dbPrecomputedSimpleStop;


public class UpdateActivity extends AppCompatActivity {

    private ProgressBar mainBar;

    private TextView text1;

    private String updateFlag;

    private ImageView mLocomotiveView;

    private ObjectAnimator mLocomotiveAnimator;

    private boolean animStart;

    private ConstraintLayout animView;

    int OFFSET = 1000;
//
   // private final String serverAdress = "10.0.2.2";
//
//    private final String port = "9000";


   // private final String serverAdress = "192.168.0.118";
   private final String serverAdress = "domin174.nazwa.pl";
    private final Integer port = 9000;

    private String getFullAdress(){
        return serverAdress +":"+port;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_update_data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            animView = (ConstraintLayout) findViewById(R.id.anim_view);
            mLocomotiveView = (ImageView) findViewById(R.id.train);
            mainBar = (ProgressBar) findViewById(R.id.progressBar);
            text1 = (TextView) findViewById(R.id.first_square);
            Path path = new Path();
            path.lineTo(0.25f, 0.25f);
            path.lineTo(0.5f, -0.25f);
            path.lineTo(0.7f, 0.5f);
            path.lineTo(0.9f, -0.75f);
            path.lineTo(1f, 1f);

            // Create the ObjectAnimatpr
            mLocomotiveAnimator = ObjectAnimator.ofFloat(mLocomotiveView,
                    View.TRANSLATION_Y,
                    -2,
                    0);

            mLocomotiveAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            mLocomotiveAnimator.setRepeatMode(ObjectAnimator.REVERSE);
            mLocomotiveAnimator.setDuration(1000);
            mLocomotiveView.setLayerType(View.LAYER_TYPE_HARDWARE,null);

            // Use the PathInterpolatorCompat
            mLocomotiveAnimator.setInterpolator(PathInterpolatorCompat.create(path));
            mLocomotiveAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                  //  mLocomotiveView.setVisibility(View.GONE);
                }
            });

            mLocomotiveAnimator.start();

            ImageView cityView = (ImageView) findViewById(R.id.city);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

            int translationXStart = ((int)displayMetrics.xdpi) - cityView.getLeft() + (int)displayMetrics.xdpi;
            int translationXEnd = -(cityView.getLeft()) - cityView.getWidth() - (int)displayMetrics.xdpi;

            ObjectAnimator mLayerAnimator = ObjectAnimator.ofFloat(cityView,
                    View.TRANSLATION_X,
                    translationXEnd,
                    translationXStart);

            mLayerAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            mLayerAnimator.setRepeatMode(ObjectAnimator.RESTART);

            mLayerAnimator.setDuration(17000);

            mLayerAnimator.start();


            ImageView cityView2 = (ImageView) findViewById(R.id.city2);


             translationXStart = ((int)displayMetrics.xdpi) - cityView2.getLeft() + (int)displayMetrics.xdpi*2+100;
             translationXEnd = -(cityView2.getLeft()) - cityView2.getWidth() - (int)displayMetrics.xdpi*2-100;

            ObjectAnimator mLayerAnimator2 = ObjectAnimator.ofFloat(cityView2,
                    View.TRANSLATION_X,
                    translationXEnd,
                    translationXStart);

            mLayerAnimator2.setRepeatCount(ObjectAnimator.INFINITE);
            mLayerAnimator2.setRepeatMode(ObjectAnimator.RESTART);

            mLayerAnimator2.setDuration(34000);

            mLayerAnimator2.start();


            ImageView wind = (ImageView) findViewById(R.id.wind);


             translationXStart = (animView.getWidth()+100) - wind.getLeft() + OFFSET;
             translationXEnd = -(wind.getLeft()+100) - wind.getWidth() - OFFSET;

            ObjectAnimator windAnimator = ObjectAnimator.ofFloat(wind,
                    View.TRANSLATION_X,
                    translationXEnd,
                    translationXStart);

            windAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            windAnimator.setRepeatMode(ObjectAnimator.RESTART);

            windAnimator.setDuration(1000);

            windAnimator.start();








            ImageView birdAnim = (ImageView) findViewById(R.id.bird_anim  );
            Drawable d = birdAnim.getDrawable();
             translationXStart = ((int)displayMetrics.xdpi) - birdAnim.getLeft();
             translationXEnd = -(birdAnim.getLeft()) - birdAnim.getWidth() - (int)displayMetrics.xdpi;

            ObjectAnimator birdAnimator = ObjectAnimator.ofFloat(birdAnim,
                    View.TRANSLATION_X,
                    translationXEnd,
                    translationXStart);

            birdAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            birdAnimator.setRepeatMode(ObjectAnimator.RESTART);

            birdAnimator.setDuration(15000);
            birdAnimator.start();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (d instanceof Animatable) {
                    ((Animatable) d).start();
                }
        }
            animStart = true;
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);


            timer =  new CountDownTimer(2000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    launchApp();
                }
            };

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                       // Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
                        Thread.yield();
                        TimeZone.setDefault(TimeZone.getTimeZone("CET"));
                        setupSharedPreferances();

                        if (checkIfServerUp()) {
                           // List<Integer> toFetch = fetchVersions();

                            fetchRestData();

                     //  updateStationData();
                           // launchApp();
                        } else {
                            //  updateStationData();
                           launchApp();
                        }
                        //  fetchConnections();
                       // TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
                    } catch (Exception ex) {
                        System.console().printf(ex.getMessage());
                    }

                }
            }).start();
        }
        catch (Exception ex){
            System.out.printf("test");
        }
    }

    private CountDownTimer timer;


    public void fetchRestData(){
        fetchVersions(new DataCallback() {
            @Override
            public void onSuccess(List<dbScheduleVersion> result) {
                try {

                    stationsFin=false;
                    conFin=false;
                    stationOnConFin=false;
                    jsonFin=false;
                    avaFin=false;


                    for (dbScheduleVersion scheduleVersion : result) {
                        fetchAvalibites(scheduleVersion.getId());
                        Thread.yield();
                        //  fetchData();
                        fetchJsonResponse(scheduleVersion.getId());
                        Thread.yield();
                    }
                    if (result==null || result.size()==0){
                        stationsFin=true;
                        conFin=false;
                        stationOnConFin=true;
                        jsonFin=true;
                        avaFin=true;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                timer.start();
                            }
                        });


                    }
                    else {
                        fetchStations();
                        Thread.yield();
                        fetchConnections();
                        Thread.yield();
                        fetchStationsOnConnection();
                        Thread.yield();
                        while (!avaFin || !jsonFin || !conFin || !stationsFin || !stationOnConFin) {
                            VolleyLog.e("ava " + avaFin + " json " + jsonFin + " con " + conFin + " statio " + stationsFin + " stationOnCon " + stationOnConFin);
                        }
                        VolleyLog.e("Finished");
                        launchApp();
                    }


                }
                catch (Exception ex){
                    VolleyLog.e(ex.getMessage());
                }
            }
        });
    }

    private boolean stationsFin;
    private boolean conFin;
    private boolean stationOnConFin;
    private boolean jsonFin;
    private boolean avaFin;


    public void fetchVersions(final DataCallback callback){

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String app_url = "http://"+getFullAdress()+"/getversions?lastdate="+dateFormat.format(new java.util.Date());
        //mainBar.isShown();


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,app_url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try{

                        }
                        catch (Exception ex){
                            Log.e("Versions",ex.getMessage());
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyLog.e("Error: ", error.getMessage());
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {


                    Type listType = new TypeToken<List<dbScheduleVersion>>(){}.getType();

                    GsonBuilder builder = new GsonBuilder();



                    builder.registerTypeAdapter(java.util.Date.class, new JsonDeserializer<java.util.Date>() {
                        public java.util.Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            return new java.util.Date(json.getAsJsonPrimitive().getAsLong());
                        }
                    });

                    Gson gson = builder.create();

                    JsonParser jsonParser = new JsonParser();
                    String json = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    JsonObject object = (JsonObject)jsonParser.parse(json);

                    List<dbScheduleVersion> list =  gson.fromJson(object.getAsJsonArray("versionsRep"), listType);

                    List<dbScheduleVersion> toAdd = new LinkedList<>();

                    for (dbScheduleVersion version : list){
                        dbScheduleVersion temp = dbScheduleVersionDAO.getStationById(version.getId());
                        if (temp==null){
                            toAdd.add(version);
                        }
                    }

                    long startTime = System.currentTimeMillis();
                    List<dbScheduleVersion> versions = new LinkedList<>();

                    ModelAdapter<dbScheduleVersion> modelAdapter = FlowManager.getModelAdapter(dbScheduleVersion.class);
                    TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                            .insertBuilder(modelAdapter).addAll(toAdd).build());

                    callback.onSuccess(toAdd);

                } catch (Exception exception) {
                    System.out.printf("test");
                }
                return Response.success(new JSONObject(),
                        HttpHeaderParser.parseCacheHeaders(response));
            }
        };



        TrainFinderApplication.getInstance(getApplicationContext()).udRequestQueue.add(req);
    }

    public void fetchData(){
        final String app_url = "http://"+getFullAdress()+"/getprecomputedparts";
        //mainBar.isShown();


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,app_url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                mainBar.setProgress(0);
//                Toast toast = Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG);
//                toast.show();
                VolleyLog.e("Error: ", error.getMessage());
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    SQLite.delete(dbPrecomputedPart.class).executeUpdateDelete();
                    SQLite.delete(dbPrecomputedStop.class).executeUpdateDelete();
                    SQLite.delete(dbPrecomputedSimpleStop.class).executeUpdateDelete();
                    SQLite.delete(dbPrecomputedConnection.class).executeUpdateDelete();

//                            mainBar.setProgress(100);
//                            VolleyLog.e("Response:%n %s", response.toString(4));
//                            Toast toast = Toast.makeText(getApplicationContext(),"We have response: "+response.toString(),Toast.LENGTH_LONG);
//                            toast.show();

                    Type listType = new TypeToken<List<dbPrecomputedPart>>(){}.getType();

                    GsonBuilder builder = new GsonBuilder();



// Register an adapter to manage the date types as long values
                    builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(json.getAsJsonPrimitive().getAsLong());
                        }
                    });

                    Gson gson = builder.create();

                    JsonParser jsonParser = new JsonParser();

                    JsonObject object = (JsonObject)jsonParser.parse(response.toString());

                    List<dbPrecomputedPart> list =  gson.fromJson(object.getAsJsonArray("precomputedParts"), listType);
                    // toast.setText(""+list.size());
                    // toast.show();


                    // List<dbPrecomputedPart> all_connetions_in_db = SQLite.select().from(dbPrecomputedPart.class).queryList();
                    long startTime = System.currentTimeMillis();
                    List<dbPrecomputedSimpleStop> simpleStops = new LinkedList<>();
                    List<dbPrecomputedConnection> connectionList = new LinkedList<>();
                    List<dbPrecomputedStop> stopList = new LinkedList<>();
                    for (dbPrecomputedPart part : list){
                        for (dbPrecomputedSimpleStop stop : part.getStopsOnStation()){
                            stop.setPartID(part.getId());
                            simpleStops.add(stop);
                        }
                        for (dbPrecomputedConnection connection : part.getConnections()){
                            connection.setPartID(part.getId());
                            for (dbPrecomputedStop precomputedStop : connection.getPrecomputedStops()){
                                precomputedStop.setPrecomputedConnectionID(connection.getId());
                                stopList.add(precomputedStop);
                            }
                            connectionList.add(connection);
                        }

                    }
                    ModelAdapter<dbPrecomputedStop> modelAdapter = FlowManager.getModelAdapter(dbPrecomputedStop.class);
                    TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                            .insertBuilder(modelAdapter).addAll(stopList).build());
                    ModelAdapter<dbPrecomputedSimpleStop> modelAdapter2 = FlowManager.getModelAdapter(dbPrecomputedSimpleStop.class);
                    TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                            .insertBuilder(modelAdapter2).addAll(simpleStops).build());
                    ModelAdapter<dbPrecomputedConnection> modelAdapter3 = FlowManager.getModelAdapter(dbPrecomputedConnection.class);
                    TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                            .insertBuilder(modelAdapter3).addAll(connectionList).build());
                    ModelAdapter<dbPrecomputedPart> modelAdapter4 = FlowManager.getModelAdapter(dbPrecomputedPart.class);
                    TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                            .insertBuilder(modelAdapter4).addAll(list).build());
                    //  toast.setText("Transaction completed in: " + (System.currentTimeMillis()
                    //          - startTime));
                    //  toast.show();
                    //  mainBar.setProgress(75);
                } catch (Exception exception) {
//                            mainBar.setProgress(25);
//                            exception.printStackTrace();
//                            Toast toast = Toast.makeText(getApplicationContext(),"Json error"+exception.getMessage(),Toast.LENGTH_LONG);
//                            toast.show();
                    System.out.printf("test");
                }
                return Response.success(new JSONObject(),
                        HttpHeaderParser.parseCacheHeaders(response));
            }
        };



        TrainFinderApplication.getInstance(getApplicationContext()).udRequestQueue.add(req);
    }

    private boolean checkIfServerUp(){
        SocketAddress sockaddr = new InetSocketAddress(serverAdress, port);
// Create your socket
        Socket socket = new Socket();
        boolean online = true;
// Connect with 10 s timeout
        try {
            socket.connect(sockaddr, 200);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void launchApp(){
        final Intent intent = new Intent(this,SearchScreen.class);
        startActivity(intent);
    }

    private void createNetwork(){
        List<dbConnection> connectionList = SQLite.select().from(dbConnection.class).queryList();
        dbStation childStation;
        dbStation parentStation;
        for (dbConnection connection : connectionList){
            for (dbStationOnConnection station : connection.getStationList()){
               SQLite.select().from(dbStation.class).where();
            }
        }
    }

    private void setupSharedPreferances() throws IOException {
        try {
            Properties properties = new Properties();
            String prop_file =  getResources().getString(R.string.prop_file);
            SharedPreferences settings = getSharedPreferences(prop_file,0);

            String this_verson_was_upadted=settings.getString("updatedMade", "DEFAULT");


            System.out.printf("|test");
        }
        catch (Exception ex){
            System.out.printf("|test");
        }
    }

    private void updateStationData(){
        stationUpdater updater = new stationUpdater();
        if (updateFlag!="updated"){
            updater.updateStationsConnetion();
        }
    }

    private void genratePaths(){

    }

    private void fetchStationsOnConnection(){
        final String app_url = "http://"+getFullAdress()+"/getstationsonconnection";
       // mainBar.isShown();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,app_url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  mainBar.setProgress(0);
              //  Toast toast = Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG);
             //   toast.show();
                VolleyLog.e("Error: ", error.getMessage());
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    String json = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    // mainBar.setProgress(100);
                   // VolleyLog.e("Response:%n %s", json);
                    VolleyLog.e("Station on conn");
                   // Toast toast = Toast.makeText(getApplicationContext(),"We have response: "+json,Toast.LENGTH_LONG);
                    //  toast.show();

                    Type listType = new TypeToken<List<dbStationOnConnection>>(){}.getType();

                    GsonBuilder builder = new GsonBuilder();

// Register an adapter to manage the date types as long values
                    builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(json.getAsJsonPrimitive().getAsLong());
                        }
                    });

                    Gson gson = builder.create();

                    JsonParser jsonParser = new JsonParser();

                    SQLite.delete(dbStationOnConnection.class).executeUpdateDelete();

                    JsonObject object = (JsonObject)jsonParser.parse(json);

                    List<dbStationOnConnection> list =  gson.fromJson(object.getAsJsonArray("stationsOnConnectionsRep"), listType);
                  //  toast.setText(""+list.size());
                    //  toast.show();

                    ModelAdapter<dbStationOnConnection> modelAdapter = FlowManager.getModelAdapter(dbStationOnConnection.class);
                    List<dbStationOnConnection> all_connetions_in_db = SQLite.select().from(dbStationOnConnection.class).queryList();
                    long startTime = System.currentTimeMillis();
                    TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                            .insertBuilder(modelAdapter).addAll(list).build());
                   // toast.setText("Transaction completed in: " + (System.currentTimeMillis()
                     //       - startTime));
                    //  toast.show();
                    //   mainBar.setProgress(75);
                } catch (Exception exception) {
                    //   mainBar.setProgress(25);
                    exception.printStackTrace();
                   // Toast toast = Toast.makeText(getApplicationContext(),"Json error"+exception.getMessage(),Toast.LENGTH_LONG);
                    //  toast.show();

                }
                stationOnConFin = true;
                return Response.success(new JSONObject(),
                        HttpHeaderParser.parseCacheHeaders(response));
            }
        };



        TrainFinderApplication.getInstance(getApplicationContext()).udRequestQueue.add(req);
    }

    private void fetchStations(){
        final String app_url = "http://"+getFullAdress()+"/getstations";
       // mainBar.isShown();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,app_url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // mainBar.setProgress(0);
                Toast toast = Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG);
               // toast.show();
                VolleyLog.e("Error: ", error.getMessage());
            }
        }){


            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    String json = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    // mainBar.setProgress(100);
                    VolleyLog.e("Response:%n %s",json);
                  //  Toast toast = Toast.makeText(getApplicationContext(),"We have response: "+json,Toast.LENGTH_LONG);
                    //   toast.show();

                    Type listType = new TypeToken<List<dbStation>>(){}.getType();

                    GsonBuilder builder = new GsonBuilder();

// Register an adapter to manage the date types as long values
                    builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(json.getAsJsonPrimitive().getAsLong());
                        }
                    });

                    Gson gson = builder.create();

                    JsonParser jsonParser = new JsonParser();

                    JsonObject object = (JsonObject)jsonParser.parse(json);
                    Thread.yield();

                    List<dbStation> list =  gson.fromJson(object.getAsJsonArray("stationsRep"), listType);
                   // toast.setText(""+list.size());
                    //   toast.show();

                    ModelAdapter<dbStation> modelAdapter = FlowManager.getModelAdapter(dbStation.class);
                    List<dbStation> all_connetions_in_db = SQLite.select().from(dbStation.class).queryList();
                    long startTime = System.currentTimeMillis();
                    TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                            .insertBuilder(modelAdapter).addAll(list).build());
                    Thread.yield();
                   // toast.setText("Transaction completed in: " + (System.currentTimeMillis()
                     //       - startTime));
                    //  toast.show();
                    //   mainBar.setProgress(75);

                } catch (Exception exception) {
                    //  mainBar.setProgress(25);
                    exception.printStackTrace();
                   // Toast toast = Toast.makeText(getApplicationContext(),"Json error"+exception.getMessage(),Toast.LENGTH_LONG);
                    //  toast.show();

                }
                stationsFin = true;
                return Response.success(new JSONObject(),
                        HttpHeaderParser.parseCacheHeaders(response));
            }
        };



        TrainFinderApplication.getInstance(getApplicationContext()).udRequestQueue.add(req);
    }

    private void fetchJsonResponse(Integer version_id){
        final Intent intent = new Intent(this,SearchScreen.class);
        final String app_url = "http://"+getFullAdress()+"/getdata?version_id="+version_id;
      //  mainBar.isShown();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,app_url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  mainBar.setProgress(0);
                Toast toast = Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG);
              //  toast.show();
                VolleyLog.e("Error: ", error.getMessage());
                //launchApp();
               // startActivity(intent);
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {

                    String json = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    //      mainBar.setProgress(100);
                   // VolleyLog.e("Response:%n %s", json);
                    VolleyLog.e("Response: arrival Hours");
                   // Toast toast = Toast.makeText(getApplicationContext(),"We have response: "+json,Toast.LENGTH_LONG);
                   // toast.show();

                    SQLite.delete(dbConnectionToHour.class).executeUpdateDelete();

                    Type listType = new TypeToken<List<dbConnectionToHour>>(){}.getType();

                    GsonBuilder builder = new GsonBuilder();
                    Thread.yield();
// Register an adapter to manage the date types as long values
                    builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            long temp = json.getAsJsonPrimitive().getAsLong();
                            DateTime dateTime = new DateTime(Long.valueOf(temp), DateTimeZone.forTimeZone(TimeZone.getTimeZone("UTC")));
                            return new Date(dateTime.toDateTime(DateTimeZone.forID("CET")).getMillis());
                        }
                    });

                    Gson gson = builder.create();

                    JsonParser jsonParser = new JsonParser();

                    JsonObject object = (JsonObject)jsonParser.parse(json);
                    Thread.yield();
                    List<dbConnectionToHour> list =  gson.fromJson(object.getAsJsonArray("connectionToHours"), listType);
                   // toast.setText(""+list.size());
                    //  toast.show();

                    SQLite.delete(dbConnectionToHour.class).executeUpdateDelete();
                    SQLite.delete(ArrivalHour.class).executeUpdateDelete();
                    Thread.yield();
                    ModelAdapter<dbConnectionToHour> modelAdapter = FlowManager.getModelAdapter(dbConnectionToHour.class);
                    List<dbConnectionToHour> all_connetions_in_db = SQLite.select().from(dbConnectionToHour.class).queryList();
                    long startTime = System.currentTimeMillis();
                    TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                            .insertBuilder(modelAdapter).addAll(list).build());
                    Thread.yield();
                    List<ArrivalHour> arrivalHours = new LinkedList<>();
                    for(dbConnectionToHour hour : list){
                        arrivalHours.addAll( hour.getConHoursList());
                    }
                    Thread.yield();
                    ModelAdapter<ArrivalHour> arrivalHoursAdapter = FlowManager.getModelAdapter(ArrivalHour.class);

                    TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                            .insertBuilder(arrivalHoursAdapter).addAll(arrivalHours).build());
                    Thread.yield();
                    //toast.setText("Transaction completed in: " + (System.currentTimeMillis()
                      //      - startTime));
                    //  toast.show();
                    // startActivity(intent);
                    //  mainBar.setProgress(100);

                } catch (Exception exception) {
                    mainBar.setProgress(25);
                    exception.printStackTrace();
                    Toast toast = Toast.makeText(getApplicationContext(),"Json error"+exception.getMessage(),Toast.LENGTH_LONG);
                   // toast.show();
                    //startActivity(intent);
                }
                jsonFin = true;
                return Response.success(new JSONObject(),
                        HttpHeaderParser.parseCacheHeaders(response));
            }
        };



        TrainFinderApplication.getInstance(getApplicationContext()).udRequestQueue.add(req);



    }

    private void fetchConnections(){
        final String app_url = "http://"+getFullAdress()+"/getconnections";
      //  mainBar.isShown();
        final Intent intent = new Intent(this,SearchScreen.class);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,app_url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  mainBar.setProgress(0);
              //  Toast toast = Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG);
              //  toast.show();
                VolleyLog.e("Error: ", error.getMessage());
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    VolleyLog.e("startParseConnection");
                    String json = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    //     mainBar.setProgress(100);
                  //  VolleyLog.e("Response:%n %s", json);
                   // Toast toast = Toast.makeText(getApplicationContext(),"We have response: "+json,Toast.LENGTH_LONG);
                    //     toast.show();

                    Type listType = new TypeToken<List<dbConnection>>(){}.getType();

                    GsonBuilder builder = new GsonBuilder();

// Register an adapter to manage the date types as long values
                    builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(json.getAsJsonPrimitive().getAsLong());
                        }
                    });

                    Gson gson = builder.create();

                    JsonParser jsonParser = new JsonParser();

                    JsonObject object = (JsonObject)jsonParser.parse(json);
                    Thread.yield();
                    List<dbConnection> list =  gson.fromJson(object.getAsJsonArray("connectionRep"), listType);
                    Thread.yield();
                    //toast.setText
                      //      (""+list.size());
                    //  toast.show();

                    SQLite.delete(dbConnection.class).executeUpdateDelete();
                    ModelAdapter<dbConnection> modelAdapter = FlowManager.getModelAdapter(dbConnection.class);
                    List<dbConnection> all_connetions_in_db = SQLite.select().from(dbConnection.class).queryList();
                    Thread.yield();
                    long startTime = System.currentTimeMillis();

                    TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                            .insertBuilder(modelAdapter).addAll(list).build());
                    Thread.yield();


                   // toast.setText("Transaction completed in: " + (System.currentTimeMillis()
                    //        - startTime));
                    //      toast.show();
                    //     mainBar.setProgress(75);
                    //  startActivity(intent);
                } catch (Exception exception) {
                    //     mainBar.setProgress(25);
                    exception.printStackTrace();
                   // Toast toast = Toast.makeText(getApplicationContext(),"Json error"+exception.getMessage(),Toast.LENGTH_LONG);
                    //   toast.show();
                    // startActivity(intent);
                }
                conFin = true;
                return Response.success(new JSONObject(),
                        HttpHeaderParser.parseCacheHeaders(response));
            }
        };



        TrainFinderApplication.getInstance(getApplicationContext()).udRequestQueue.add(req);
    }


    private void fetchAvalibites(Integer version_id){
        final String app_url = "http://"+getFullAdress()+"/getmappings?version_id="+version_id;
      //  mainBar.isShown();
        final Intent intent = new Intent(this,SearchScreen.class);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,app_url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  mainBar.setProgress(0);
               // Toast toast = Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG);
              //  toast.show();
                VolleyLog.e("Error: ", error.getMessage());
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {

                    //   mainBar.setProgress(100);
                    VolleyLog.e("startParseAva");
                    String json = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                   // VolleyLog.e("Response:%n %s", json);
                  //  Toast toast = Toast.makeText(getApplicationContext(),"We have response: "+json,Toast.LENGTH_LONG);
                    //   toast.show();

                    Type listType = new TypeToken<List<dbConnectionMapping>>(){}.getType();
                    Thread.yield();
                    GsonBuilder builder = new GsonBuilder();
                    final SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
// Register an adapter to manage the date types as long values
                    builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            try {
                                return new Date(inFormat.parse(json.getAsJsonPrimitive().getAsString()).getTime());

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    });

                    Gson gson = builder.create();
                    Thread.yield();
                    JsonParser jsonParser = new JsonParser();

                    JsonObject object = (JsonObject)jsonParser.parse(json);
                    Thread.yield();
                    List<dbConnectionMapping> list =  gson.fromJson(object.getAsJsonArray("mappings"), listType);
                    List<dbConnectionAvalabities> toAdd = new LinkedList<>();
                    for (dbConnectionMapping mapping : list){
                        toAdd.addAll(mapping.getAvailabilities());
                    }
                    Thread.yield();
                   // toast.setText
                     //       (""+list.size());
                    //  toast.show();

                    //SQLite.delete(dbConnectionMapping.class).executeUpdateDelete();
                    Thread.yield();
                    //SQLite.delete(dbConnectionAvalabities.class).executeUpdateDelete();
                    Thread.yield();
                    ModelAdapter<dbConnectionMapping> modelAdapter = FlowManager.getModelAdapter(dbConnectionMapping.class);
                    Thread.yield();
                    ModelAdapter<dbConnectionAvalabities> modelAdapter2 = FlowManager.getModelAdapter(dbConnectionAvalabities.class);
                    List<dbConnectionMapping> all_connetions_in_db = SQLite.select().from(dbConnectionMapping.class).queryList();
                    long startTime = System.currentTimeMillis();

                    TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                            .insertBuilder(modelAdapter).addAll(list).build());
                    TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
                            .insertBuilder(modelAdapter2).addAll(toAdd).build());
                    Thread.yield();

                   // toast.setText("Transaction completed in: " + (System.currentTimeMillis()
                     //       - startTime));
                    //   toast.show();
                    //  mainBar.setProgress(75);
                    //  startActivity(intent);
                } catch (Exception exception) {
                    //  mainBar.setProgress(25);
                    exception.printStackTrace();
                    Toast toast = Toast.makeText(getApplicationContext(),"Json error"+exception.getMessage(),Toast.LENGTH_LONG);
                    //    toast.show();
                    // startActivity(intent);
                }
                avaFin = true;
                return Response.success(new JSONObject(),
                        HttpHeaderParser.parseCacheHeaders(response));
            }
        };



        TrainFinderApplication.getInstance(getApplicationContext()).udRequestQueue.add(req);
    }





}
