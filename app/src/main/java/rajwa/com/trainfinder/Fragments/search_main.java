package rajwa.com.trainfinder.Fragments;

import rajwa.com.trainfinder.DAOs.dbStationDAO;
import rajwa.com.trainfinder.Model.dbStation;
import rajwa.com.trainfinder.Model.loaders.DataStorage;
import rajwa.com.trainfinder.R;
import rajwa.com.trainfinder.Activities.ResultDisplay;
import rajwa.com.trainfinder.SearchScreen;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import hirondelle.date4j.DateTime;


public class search_main extends Fragment {

    private AutoCompleteTextView fromText;

    private Button searchButton;

    private AutoCompleteTextView toText;

    private TextView timePicker;

    private TextView datePicker;

    private Spinner numberOfSwitches;

    public Date getChoosenDate() {
        return choosenDate;
    }

    private Date choosenDate;


    public search_main() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       // try {

            View v = inflater.inflate(R.layout.fragment_search_main, container, false);

        datePicker = (TextView) v.findViewById(R.id.textView7);

        timePicker = (TextView) v.findViewById(R.id.textView6);

        choosenDate = new Date();
        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        String s = formatter.format(choosenDate);
        DateTime time = new DateTime(s);
        String timePickerText = time.format("hh:mm");
        String datePickerText = time.format("YYYY-MM-DD");

        timePicker.setText(timePickerText);
        datePicker.setText(datePickerText);

            String[] arraySpinner = new String[]{
                    "0", "1", "2", "3"
            };
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this.getActivity(),
                    android.R.layout.simple_spinner_item, arraySpinner);

            numberOfSwitches = (Spinner) v.findViewById(R.id.spinner);
            numberOfSwitches.setAdapter(spinnerAdapter);

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fromText.getText()!=null && toText.getText()!=null) {
                        Editable editable = fromText.getText();
                        fromText.setText(toText.getText());
                        toText.setText(editable);
                    }
                }
            });


            timePicker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new SingleDateAndTimePickerDialog.Builder(v.getContext())
                            .bottomSheet().curved().minutesStep(15).defaultDate(choosenDate)
                            .listener(new SingleDateAndTimePickerDialog.Listener() {
                                @Override
                                public void onDateSelected(Date date) {
                                    Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    String s = formatter.format(date);
                                    DateTime time = new DateTime(s);
                                    String timePickerText = time.format("hh:mm");
                                    String datePickerText = time.format("YYYY-MM-DD");

                                    try {
                                        timePicker.setText(timePickerText);
                                        datePicker.setText(datePickerText);
                                        choosenDate = date;
                                    } catch (Exception ex) {
                                        System.out.printf("test");
                                    }
                                }
                            })
                            .display();
                }
            });
        datePicker.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              new SingleDateAndTimePickerDialog.Builder(v.getContext())
                                                      .bottomSheet().curved().minutesStep(15).defaultDate(choosenDate)
                                                      .listener(new SingleDateAndTimePickerDialog.Listener() {
                                                          @Override
                                                          public void onDateSelected(Date date) {
                                                              Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                              String s = formatter.format(date);
                                                              DateTime time = new DateTime(s);
                                                              String timePickerText = time.format("hh:mm");
                                                              String datePickerText = time.format("YYYY-MM-DD");

                                                              try {
                                                                  timePicker.setText(timePickerText);
                                                                  datePicker.setText(datePickerText);
                                                                  choosenDate = date;
                                                              } catch (Exception ex) {
                                                                  System.out.printf("test");
                                                              }
                                                          }
                                                      })
                                                      .display();
                                          }

                                          ;
                                      });

            List<dbStation> stations = SQLite.select().from(dbStation.class).queryList();
            String[] stationsArray = new String[stations.size()];
            int i = 0;
            for (dbStation station : stations) {

                stationsArray[i] = station.stationName;
                i++;
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_dropdown_item_1line, stationsArray);

            searchButton = (Button) v.findViewById(R.id.searchButton);
            implementButtonListenrs();

            toText = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteToEdit);
            setupAutoComplete(adapter);

            fromText = (AutoCompleteTextView) v.findViewById(R.id.autoCompleteFromEdit);
            fromText.setThreshold(0);
            fromText.setAdapter(adapter);
            fromText.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP && !v.hasFocus()) {
                        v.performClick();
                    }
                    return false;
                }
            });

            fromText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    fromText.showDropDown();
                }
            });

            fromText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        hideKeyboard();
                    }
                }
            });

            return v;

//        catch (Exception ex){
//            System.out.printf("test");
//        }
//        return null;
    }







    private void setupAutoComplete(ArrayAdapter<String> apadter){
        toText.setAdapter(apadter);
        toText.setThreshold(0);

        toText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP && !v.hasFocus()) {
                    v.performClick();
                }
                return false;
            }
        });

        toText.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(final View arg0){
                fromText.showDropDown();
            }
        });

        toText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    hideKeyboard();
                }
            }
        });
    }

    private void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager)this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(fromText.getWindowToken(),0);
    }

    private void implementButtonListenrs(){


        final Context context = this.getContext();
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    if ((((SearchScreen)getActivity()).loadingFinished)) {
                        if (dbStationDAO.getStationByName(fromText.getText().toString())!=null && dbStationDAO.getStationByName(toText.getText().toString())!=null) {
                            Bundle bundle = new Bundle();
                            Intent intent = new Intent(getActivity(), ResultDisplay.class);

                            DataStorage storage = new DataStorage(((SearchScreen) getActivity()).views);
//                        bundle.putSerializable("partData", storage);
//                        intent.putExtras(bundle);
                            intent.putExtra("startStation", fromText.getText().toString());
                            intent.putExtra("endStation", toText.getText().toString());
                            intent.putExtra("partData", storage);
                            intent.putExtra("currentDate", String.valueOf(getChoosenDate().getTime()));
                            String text = fromText.getText().toString();
                            String text1 = toText.getText().toString();
                            startActivity(intent);
                        }
                        else {
                            Toast toast = Toast.makeText(context,"Niepoprawna nazwa stacji",Toast.LENGTH_LONG);
                            toast.show();
                        }
                    }
                    else {
                        Toast toast = Toast.makeText(context,"Trwa ladowanie",Toast.LENGTH_LONG);
                        toast.show();
                    }

                }
                catch (Exception ex){
                   Toast toast = Toast.makeText(context,"Json error"+ex.getMessage(),Toast.LENGTH_LONG);
                    toast.show();
                }

            }
        });
    }
}
