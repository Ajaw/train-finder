package rajwa.com.trainfinder.DAOs;

import java.util.List;

import rajwa.com.trainfinder.Model.*;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import rajwa.com.trainfinder.Model.dbStation;

/**
 * Created by Dominik on 07.04.2017.
 */

public  class  dbStationDAO {

    public static List<dbStation> getAllStaions(){
        return SQLite.select().from(dbStation.class).queryList();
    }

    public static dbStation getStationById(int id){
        return SQLite.select().from(dbStation.class).where(dbStation_Table.id.eq(id)).queryList().get(0);
   }

    public static dbStation getStationByName(String name){
        return SQLite.select().from(dbStation.class).where(dbStation_Table.stationName.eq(name)).queryList().get(0);
    }



}
