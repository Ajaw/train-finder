package rajwa.com.trainfinder.DAOs;

import rajwa.com.trainfinder.Model.*;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import rajwa.com.trainfinder.Model.dbConnection;

/**
 * Created by Dominik on 07.04.2017.
 */

public class dbConnectionDAO {

    public static dbConnection getConnectionById(int id){
        return SQLite.select().from(dbConnection.class).where(dbConnection_Table.id.eq(id)).queryList().get(0);
    }

}
