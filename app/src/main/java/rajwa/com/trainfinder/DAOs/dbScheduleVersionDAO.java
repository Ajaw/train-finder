package rajwa.com.trainfinder.DAOs;

import rajwa.com.trainfinder.Model.dbScheduleVersion;
import rajwa.com.trainfinder.Model.dbScheduleVersion_Table;

import com.raizlabs.android.dbflow.sql.language.SQLite;

/**
 * Created by Dominik on 04.12.2017.
 */

public class dbScheduleVersionDAO {

    public static dbScheduleVersion getStationById(int id){
        try {


            return SQLite.select().from(dbScheduleVersion.class).where(dbScheduleVersion_Table.id.eq(id)).queryList().get(0);
        }
        catch (Exception ex){
            return null;
        }
    }

}
