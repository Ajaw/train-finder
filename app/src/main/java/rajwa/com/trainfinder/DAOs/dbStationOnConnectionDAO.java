package rajwa.com.trainfinder.DAOs;


import rajwa.com.trainfinder.Model.*;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import rajwa.com.trainfinder.Model.dbStationOnConnection;

/**
 * Created by Dominik on 07.04.2017.
 */

public class dbStationOnConnectionDAO {

    public static dbStationOnConnection getStationOnConnectionByID(int id){
        return SQLite.select().from(dbStationOnConnection.class).where(dbStationOnConnection_Table.id.eq(id)).queryList().get(0);
    }

    public static String getMotherStationNameByStationOnConnectionID(int id){
        dbStationOnConnection stationOnConnection = SQLite.select().from(dbStationOnConnection.class).where(dbStationOnConnection_Table.id.eq(id)).queryList().get(0);
        return dbStationDAO.getStationById(stationOnConnection.dbStationId).stationName;
    }

//
}
