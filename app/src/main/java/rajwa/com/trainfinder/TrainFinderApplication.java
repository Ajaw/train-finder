package rajwa.com.trainfinder;

import android.app.Application;
import rajwa.com.trainfinder.Database.TrainDB;
import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.facebook.stetho.DumperPluginsProvider;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.dumpapp.DumperPlugin;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.util.ArrayList;

/**
 * Created by Dominik on 16.03.2017.
 */

public class TrainFinderApplication extends Application {

    public RequestQueue udRequestQueue;

    public DatabaseDefinition database;

    @Override
    public void onCreate(){
        super.onCreate();
        final Context context  = this;
        Stetho.initialize(
                Stetho.newInitializerBuilder(context).enableDumpapp(new SampleDumperPliginsProvider(context))
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(context))
                .build()
                );


        FlowManager.init(new FlowConfig.Builder(this).build());

        udRequestQueue = Volley.newRequestQueue(getApplicationContext());
        VolleyLog.setTag("Volley");

        Log.isLoggable("Volley",Log.VERBOSE);

        database = FlowManager.getDatabase(TrainDB.class);

//        Transaction transaction = database.beginTransactionAsync(new ITransaction() {
//            @Override
//            public void execute(DatabaseWrapper databaseWrapper) {
//                called.
//            }
//        });
        instance = this;

    }

    public static synchronized TrainFinderApplication getInstance(Context context){
        if (instance== null){
            instance = new TrainFinderApplication();
        }
        return instance;
    }

    public static TrainFinderApplication instance;
    private static class SampleDumperPliginsProvider implements DumperPluginsProvider{
        private final Context mContext;

        public SampleDumperPliginsProvider(Context context)
        {
            mContext = context;
        }

        @Override
        public Iterable<DumperPlugin> get(){
            ArrayList<DumperPlugin> plugins = new ArrayList<>();
            for ( DumperPlugin plugin : Stetho.defaultDumperPluginsProvider(mContext).get()) {
                plugins.add(plugin);
            }
            return plugins;
        }
    }

}



